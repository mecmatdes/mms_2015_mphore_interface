#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of double-well-potential package that generates
# the double well used in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# double-well-potential is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# double-well-potential is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import numpy as np

# Morse Potential
def morsepot(D,alpha,r0,r):
    """ Return the potential due to morse potential
        input:
            D     - potential depth
            alpha - inverse potential well length
            r0    - equilibrium distance
            r     - distance at which the potential needs to be evaluated
        output:
            potential value
    """
    return D*(np.exp(-2*alpha*(r-r0))-2.0*np.exp(-alpha*(r-r0)));

# Derivative of the Morse Potential
def dermorsepot(D,alpha,r0,r):
    """ Return the derivative of the potential the force due to an morse potential
        input:
            D     - potential depth
            alpha - inverse potential well length
            r0    - equilibrium distance
            r     - distance at which the potential needs to be evaluated
        output:
            derivative value (applied force -> gradient of potential w.r.t r)
    """
    return -2*alpha*D*(np.exp(-2*alpha*(r-r0))-np.exp(-alpha*(r-r0)));

# Second Derivative of the Morse Potential
def ddermorsepot(D,alpha,r0,r):
    """ return the second derivative of the potential the force due to an morse potential
        input:
            D     - potential depth
            alpha - inverse potential well length
            r0    - equilibrium distance
            r     - distance at which the potential needs to be evaluated
        output:
            second derivative value 
    """
    return 2*alpha*alpha*D*(2*np.exp(-2*alpha*(r-r0))-np.exp(-alpha*(r-r0)));
