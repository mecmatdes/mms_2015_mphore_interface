#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of double-well-potential package that generates
# the double well used in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# double-well-potential is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# double-well-potential is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import cubicbezier
import morse
import scipy.optimize as sco

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
matplotlib.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica'],'size':32})
matplotlib.rc('text', usetex=True)

# Parameter definitions
# Potential parameters
def param():

    # Control Surface
    points=np.array([[4.0,4.5,6.0,9.0,10,11.5,12.5,12.6,13.5,15,17],
        [-10,-30,-60,-52,-30,0,0,-58,-55,-45,-25]])
    points[0,:]=points[0,:]
    
    # Knots
    knots=np.array(range(points.shape[1]+2))

    # Get the ends
    samples,vals,dvals,ddvals,clist=cubicbezier.cbs(points,knots,2)
    dydx=dvals[1,:]/dvals[0,:]
    d2ydx2=(ddvals[1,:]-dydx*ddvals[0,:])/(dvals[0,:]*dvals[0,:])

    # solve for morse parameters flanking the double well
    r0=np.zeros(2)
    D0=np.zeros(2)
    alpha=np.zeros(2)

    r=vals[0,0]
    a=vals[1,0]
    b=dydx[0]
    c=d2ydx2[0]
    alpha[0]=(-3*b-np.sqrt(9*b*b-8*a*c))/(4*a)
    r0[0]=r-np.log(0.5*(b+2*alpha[0]*a)/(b+alpha[0]*a))/alpha[0]
    D0[0]=a/(np.exp(-2*alpha[0]*(r-r0[0]))-2.0*np.exp(-alpha[0]*(r-r0[0])))

    r=vals[0,-1]
    a=vals[1,-1]
    b=dydx[-1]
    c=d2ydx2[-1]
    alpha[1]=(-3*b-np.sqrt(9*b*b-8*a*c))/(4*a)
    r0[1]=r-np.log(0.5*(b+2*alpha[1]*a)/(b+alpha[1]*a))/alpha[1]
    D0[1]=a/(np.exp(-2*alpha[1]*(r-r0[1]))-2.0*np.exp(-alpha[1]*(r-r0[1])))

    del r,a,b,c,samples,dvals,ddvals

    # compute positions
    # Minima 1, Minima 2, Maximum , Max Force 
    # Pos
    # Function value
    # Derivative value
    dbb=np.zeros([3,4])
    res=sco.minimize(cubicbezier.wrapcbsyofx,7,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,0]=res.x[0]
    dbb[1,0]=res.fun
    dbb[2,0]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsyofx,13,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,1]=res.x[0]
    dbb[1,1]=res.fun
    dbb[2,1]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsnyofx,11,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,2]=res.x[0]
    dbb[1,2]=-res.fun
    dbb[2,2]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsndyofx,11,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,3]=res.x[0]
    dbb[1,3]=-res.fun
    dbb[2,3]=-res.jac

    return [points,clist,knots,r0,D0,alpha,dbb]

# Writing the Double well potential
def writedoublewell(numsamples=101,potfile='pot.dat'):

    [points,clist,knots,r0,D0,alpha,dbb]=param()

    # Starting point is 0.1
    xl=np.linspace(0.1,clist[0],numsamples,endpoint=False)
    yl=morse.morsepot(D0[0],alpha[0],r0[0],xl)
    dyl=morse.dermorsepot(D0[0],alpha[0],r0[0],xl)

    samples,vals,dvals,ddvals,clist=cubicbezier.cbs(points,knots,numsamples)
    xm=vals[0,:]
    ym=vals[1,:]
    dym=dvals[1,:]/dvals[0,:]

    xr=np.linspace(clist[-1],clist[-1]*2,2*numsamples+1)
    xr=np.delete(xr,0)
    yr=morse.morsepot(D0[1],alpha[1],r0[1],xr)
    dyr=morse.dermorsepot(D0[1],alpha[1],r0[1],xr)

    f=open(potfile,'w')
    f.write('#Double Well Potential\n\n')
    f.write('DOUBLE\n')
    f.write('N %d \n\n'%(numsamples+2*numsamples+xm.shape[0]))
    count=0
    for iind in range(numsamples):
        count=count+1
        f.write('%d %4.5f %4.5f %4.5f\n'%(count,xl[iind],yl[iind],-dyl[iind]))
    for iind in range(xm.shape[0]):
        count=count+1
        f.write('%d %4.5f %4.5f %4.5f\n'%(count,xm[iind],ym[iind],-dym[iind]))
    for iind in range(2*numsamples):
        count=count+1
        f.write('%d %4.5f %4.5f %4.5f\n'%(count,xr[iind],yr[iind],-dyr[iind]))
    f.close()
    return count

# Plotting the Double well potential
def plotdoublewell(numsamples=101,plotfile='potential.png'):

    [points,clist,knots,r0,D0,alpha,dbb]=param()

    xl=np.linspace(0.1,clist[0],numsamples,endpoint=False)
    yl=morse.morsepot(D0[0],alpha[0],r0[0],xl)
    dyl=morse.dermorsepot(D0[0],alpha[0],r0[0],xl)

    samples,vals,dvals,ddvals,clist=cubicbezier.cbs(points,knots,numsamples)
    xm=vals[0,:]
    ym=vals[1,:]
    dym=dvals[1,:]/dvals[0,:]

    xr=np.linspace(clist[-1],clist[-1]*2,2*numsamples+1)
    xr=np.delete(xr,0)
    yr=morse.morsepot(D0[1],alpha[1],r0[1],xr)
    dyr=morse.dermorsepot(D0[1],alpha[1],r0[1],xr)

    plt.figure(1,figsize=(12,8))
    plt.plot(xl,yl,linewidth=3,color='blue')
    plt.plot(xm,ym,linewidth=3,color='blue')
    plt.plot(xr,yr,linewidth=3,color='blue')
    plt.xlabel(r'Distance \AA',fontsize=48)
    plt.ylabel(r'Potential kcal/mol',fontsize=48)
    plt.grid(True)
    plt.tight_layout()

    plt.savefig(plotfile,dpi=128,format='png')


if __name__ == '__main__':
     
    [points,clist,knots,r0,D0,alpha,dbb]=param()
    writedoublewell()
    plotdoublewell()