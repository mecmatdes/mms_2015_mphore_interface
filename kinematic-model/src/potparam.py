#!/usr/bin/env python

import numpy as np
import cubicbezier
import morse
import scipy.optimize as sco

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

def paramconvfac(frac=0.05):
    """ Function that provides the parameters
    Input:
        frac - fraction of LJ energy interactions between substrate particles to be used for nonbonded interaction between substrate and mechanophore beads
    Output:
        points      - cubic Bezier spline control points
        clist       - points denoting end of cubic spline
        knots       - cubic Bezier knot points
        r0,D0,alpha - Morse potential parameters flanking the double well
        dbb         - extremal values of double well
        auwall      - substrate wall lj 9-3 parameters
        auau        - intra substrate lj 12-6 parameters
        subsme      - substrate mechanophore lj 12-6 parameters
        subsat      - morse parameters of attachment
    """

    # L J Parameters for auau and the walls
    auau=np.zeros(4)
    #eps0 sigma cutoff r0
    auau[0]=12.306 #kcal/mol #eps0
    auau[3]=2.8847 #Angstroms #r0
    auau[1]=auau[3]/(2.0**(1.0/6.0)) #Angstroms #sigma
    auau[2]=2.5*auau[3] #cutoff

    num_density=4.0/((auau[3]*np.sqrt(2))**3)

    subsat=4 #Angs

    # Mphore points
    points=np.array([[4.0,4.5,6.0,9.0,10,11.5,12.5,12.6,13.5,15,17],
        [-10,-30,-60,-52,-30,0,0,-58,-55,-45,-25]])

    knots=np.array(range(points.shape[1]+2))

    # Samples
    samples,vals,dvals,ddvals,clist=cubicbezier.cbs(points,knots,2)
    dydx=dvals[1,:]/dvals[0,:]
    d2ydx2=(ddvals[1,:]-dydx*ddvals[0,:])/(dvals[0,:]*dvals[0,:])

    # solve for morse parameters flanking the double well
    r0=np.zeros(2)
    D0=np.zeros(2)
    alpha=np.zeros(2)

    r=vals[0,0]
    a=vals[1,0]
    b=dydx[0]
    c=d2ydx2[0]
    alpha[0]=(-3*b-np.sqrt(9*b*b-8*a*c))/(4*a)
    r0[0]=r-np.log(0.5*(b+2*alpha[0]*a)/(b+alpha[0]*a))/alpha[0]
    D0[0]=a/(np.exp(-2*alpha[0]*(r-r0[0]))-2.0*np.exp(-alpha[0]*(r-r0[0])))

    r=vals[0,-1]
    a=vals[1,-1]
    b=dydx[-1]
    c=d2ydx2[-1]
    alpha[1]=(-3*b-np.sqrt(9*b*b-8*a*c))/(4*a)
    r0[1]=r-np.log(0.5*(b+2*alpha[1]*a)/(b+alpha[1]*a))/alpha[1]
    D0[1]=a/(np.exp(-2*alpha[1]*(r-r0[1]))-2.0*np.exp(-alpha[1]*(r-r0[1])))

    del r,a,b,c,samples,dvals,ddvals

    # compute positions
    # Minima 1, Minima 2, Maximum , Force for Delta E = 0
    # Pos
    # Function value
    # Derivative value
    dbb=np.zeros([3,4])
    res=sco.minimize(cubicbezier.wrapcbsyofx,7,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,0]=res.x[0]
    dbb[1,0]=res.fun
    dbb[2,0]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsyofx,13,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,1]=res.x[0]
    dbb[1,1]=res.fun
    dbb[2,1]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsnyofx,11,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,2]=res.x[0]
    dbb[1,2]=-res.fun
    dbb[2,2]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsndyofx,11,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,3]=res.x[0]
    dbb[1,3]=-res.fun
    dbb[2,3]=-res.jac

    # Non bonded interaction
    subsme=np.zeros(4)
    subsme[0]=frac*auau[0] #kcal/mol #eps0
    subsme[3]= 3.5 #Angstroms #distance # Vanderwaals radius of gold and carbon
    subsme[1]=subsme[3]/(2.0**(1.0/6.0)) #Angstroms #sigma
    subsme[2]=2.5*subsme[3] #cutoff

    auwall=np.zeros(4)
    #eps0 sigma cutoff walldist
    #This is generated based on the weight of the non-bonded interaction
    auwall[0]=(2.0/3.0)*np.pi*num_density*(subsme[1]**3)*subsme[0] #eps0
    auwall[1]=subsme[1] #sigma
    auwall[2]=subsme[2] #cutoff
    auwall[3]=auwall[1]/(2.5**(1.0/6.0))

    return [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,subsme,subsat]



