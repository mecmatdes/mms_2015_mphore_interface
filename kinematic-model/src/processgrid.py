#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import glob
import numpy as np
import sys

if __name__ == '__main__':
    
    if(len(sys.argv)!=4):
        print("Usage: %s totgrid xsize ysize")
        exit()
        
    totgrid=np.int(sys.argv[1])
    xsize=np.int(sys.argv[2])
    ysize=np.int(sys.argv[3])
    
    numx=np.int(totgrid/xsize)
    numy=np.int(totgrid/ysize)
    
    actflag=np.zeros([totgrid,totgrid])
    disflag=np.zeros([totgrid,totgrid])

    actlist=glob.glob('actflag[0-9]*.txt')
    dislist=glob.glob('disflag[0-9]*.txt')

    words=[]
    beg=np.zeros(2)

    for fil in range(len(actlist)):
        with open(actlist[fil],'r') as f:
            words=f.readline().split()
        beg[0]=np.int(words[1])
        beg[1]=np.int(words[2])
        actflag[beg[0]:beg[0]+numx,beg[1]:beg[1]+numy]=np.loadtxt(actlist[fil])
    
    for fil in range(len(dislist)):
        with open(dislist[fil],'r') as f:
            words=f.readline().split()
        beg[0]=np.int(words[1])
        beg[1]=np.int(words[2])
        disflag[beg[0]:beg[0]+numx,beg[1]:beg[1]+numy]=np.loadtxt(dislist[fil])

    np.savetxt('actflag.txt',actflag,fmt='%d')
    np.savetxt('disflag.txt',disflag,fmt='%d')
