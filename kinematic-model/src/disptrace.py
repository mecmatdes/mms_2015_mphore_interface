#!/usr/bin/env python

from mpi4py import MPI
import numpy as np
import potparam
import scipy.optimize as sco
import sys
import potential
import normpot

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.



if __name__ == '__main__':

    # MPI world details
    comm=MPI.COMM_WORLD
    rank=comm.Get_rank()
    size=comm.Get_size()

    # Input verification
    if(len(sys.argv)!=6):
        if(rank==0):
            print("""
Error: %s frac numangle numsteps alpha D
frac     -- fraction of substrate LJ potential
numangle -- number of angles to average 
numsteps --  number of steps to displace 
alpha,D  -- attachment potential parameters in normalized terms
              """%(sys.argv[0]))
        exit()
    
    frac=np.double(sys.argv[1])   
    numangle=np.int(sys.argv[2])
    numsteps=np.int(sys.argv[3])

    #Domain Decomposition
    anglepproc=numangle/size
    count=0
    offset=0
    for iind in range(size):
        if(rank==count):
            offset=iind*anglepproc
        count=count+1

    # Parameters
    [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,subsme,subsat]=potparam.paramconvfac(frac)

    # Normalizing
    [lscale,escale,nbnds,nr0,nD0,nalpha,npoints,nclist,nauwall,natbnd]=normpot.normparam(dbb,r0,D0,alpha,points,clist,auwall,subsat)

    # User created distances
    # Distance along z +- inter layer spacing
    nwallpos=np.array([-auau[3]*np.sqrt(2.0/3.0),auau[3]*np.sqrt(2.0/3.0)])/lscale
    
    # Attachment alpha in normalized coordinates
    natalpha=np.float(sys.argv[4])

    # Attachment D in normalized coordinates
    natD=np.float(sys.argv[5])

    # Disociation bnd_length
    disbndlength=natbnd+3.0*np.log(2)/natalpha

    # Total bond length
    # ---[-]---[-]----
    ntb=nbnds[0]+2*natbnd

    # Distance to shear
    d2shear=2*ntb+max((nbnds[1]-nbnds[0]),5.0*np.log(2)/natalpha)

    # Velocity of shear
    velocity=(d2shear)/(2*numsteps)

    # File handles for local output
    f1=open('bonddist%d.txt'%(rank),'w')
    f2=open('actlog%d.txt'%(rank),'w')
    f3=open('dislog%d.txt'%(rank),'w')

    # Generic Initializations
    bond_dist=np.zeros(3)
    act_flag=np.zeros([numsteps,anglepproc],dtype=np.int)
    dis_flag=np.zeros([numsteps,anglepproc],dtype=np.int)
    bond_orient=np.zeros(numsteps)
    bond_act_orient=np.zeros(numsteps)

    # Specific Initializations and writing in rank0
    if(rank==0):

        # Save the interface displacements
        np.savetxt("ifacedist.txt",np.arange(numsteps)*velocity*lscale*2,fmt='%4.5f')

        # Generate the thetas and send it to all others
        angle=np.arcsin((nwallpos[1]-nwallpos[0])/(dbb[0,0]/lscale))
        #angle=np.arcsin(2.0*auau[3]*np.sqrt(2.0/3.0)/dbb[0,0])

        thetaset=np.random.rand(numangle)*angle
        phiset=np.random.rand(numangle)*2*np.pi

        # Save the angles
        np.savetxt("theta.txt",thetaset,fmt='%4.5f')
        np.savetxt("phi.txt",phiset,fmt='%4.5f')


    # Specific Initializations in other ranks
    else:

        # Theta is initialized to zeros
        thetaset=np.zeros(numangle)
        phiset=np.zeros(numangle)

    # Broadcast this array to all processors
    comm.Bcast([thetaset,MPI.DOUBLE],root=0)
    comm.Bcast([phiset,MPI.DOUBLE],root=0)

    # For each theta get the activation behavior
    for tind in range(anglepproc):

        theta=thetaset[offset+tind]
        phi=phiset[offset+tind]

        # Files - enter angles
        f1.write('Angle %f %f\n\n'%(theta,phi))
        f2.write('Angle %f %f\n\n'%(theta,phi))
        f3.write('Angle %f %f\n\n'%(theta,phi))

        ends = np.array([-np.cos(theta)*np.cos(phi),-np.cos(theta)*np.sin(phi),-np.sin(theta),np.cos(theta)*np.cos(phi),np.cos(theta)*np.sin(phi),np.sin(theta)])*nbnds[0]/2.0
        radius = np.sqrt(natbnd**2 - (ends[2]-nwallpos[0])**2)
        ang = np.random.rand()*np.pi*2.0
        ends[0] = ends[0] + radius*np.cos(ang)
        ends[1] = ends[1] + radius*np.sin(ang)
        ends[2] = nwallpos[0]

        radius = np.sqrt(natbnd**2 - (nwallpos[1]-ends[5])**2)
        ang = np.random.rand()*np.pi*2.0
        ends[3] = ends[3] + radius*np.cos(ang)
        ends[4] = ends[4] + radius*np.sin(ang)
        ends[5] = nwallpos[1]

        start = np.array([-np.cos(theta)*np.cos(phi),-np.cos(theta)*np.sin(phi),-np.sin(theta),np.cos(theta)*np.cos(phi),np.cos(theta)*np.sin(phi),np.sin(theta)])*nbnds[0]/2.0
        scale = np.min(np.log(2)/natalpha/5.0,0.1)

        start = start + (np.random.rand(6)-0.5)*scale

        for kind in range(numsteps):

            basestart = start
            debug = False

            ret = sco.fmin_tnc(func=potential.potenergy,x0=start,args=(ends,nD0,nalpha,nr0,npoints,knots,nclist,natD,natalpha,natbnd,nauwall[0],nauwall[1],nwallpos),approx_grad=False,bounds=None,xtol=1e-10,ftol=1e-10,messages=0,maxfun=100)
            start = ret[0]

            if(ret[2]<0 or ret[2]>2):

                #print("L-BFGS-B")
                ret = sco.minimize(potential.potenergy,x0=start,args=(ends,nD0,nalpha,nr0,npoints,knots,nclist,natD,natalpha,natbnd,nauwall[0],nauwall[1],nwallpos),method='L-BFGS-B',jac=True,options={'gtol':1e-10,'maxiter': 1000, 'disp': False})
                start=ret.x

                if(ret.success==False):

                    #print("Nelder")
                    ret = sco.minimize(potential.potenergy,x0=start,args=(ends,nD0,nalpha,nr0,npoints,knots,nclist,natD,natalpha,natbnd,nauwall[0],nauwall[1],nwallpos),method='Nelder-Mead',jac=True,options={'xtol':1E-10,'ftol':1E-10, 'maxiter': 10000, 'maxfev':10000, 'disp': False})
                    start = ret.x

                    if(ret.success==False):
                        debug=True
                        start=basestart

            bond_dist[0]=np.sqrt(np.sum((start[:3]-ends[:3])**2))
            diff=start[3:]-start[:3]
            bond_dist[1]=np.sqrt(np.sum(diff**2))
            bond_dist[2]=np.sqrt(np.sum((ends[3:]-start[3:])**2))

            bond_orient[kind]=bond_orient[kind]+(diff[0]**2)/np.sum(diff**2)

            if(bond_dist[1]>nbnds[2]):
                act_flag[kind,tind]=1
                bond_act_orient[kind]=bond_act_orient[kind]+(diff[0]**2)/np.sum(diff**2)

            if(bond_dist[0]>disbndlength or bond_dist[2]>disbndlength):
                dis_flag[kind,tind]=1

            ends[0]=ends[0]-velocity
            ends[3]=ends[3]+velocity

            # Write local files
            f1.write('%4.5f %4.5f %4.5f\n'%(bond_dist[0]*lscale,bond_dist[1]*lscale,bond_dist[2]*lscale))
            f2.write('%d\n'%(act_flag[kind,tind]))
            f3.write('%d\n'%(dis_flag[kind,tind]))

        f1.write('\n')
        f2.write('\n')
        f3.write('\n')

        f1.flush()
        f2.flush()
        f3.flush()

    f1.close()
    f2.close()
    f3.close()

    np.savetxt('actflag%d.txt'%(rank),np.sum(act_flag,1),fmt='%d')
    np.savetxt('disflag%d.txt'%(rank),np.sum(dis_flag,1),fmt='%d')
    np.savetxt('cossqphi%d.txt'%(rank),bond_orient/(anglepproc*1.0),fmt='%4.4f')
    np.savetxt('actcossqphi%d.txt'%(rank),bond_act_orient/np.sum(act_flag,1),fmt='%4.4f')