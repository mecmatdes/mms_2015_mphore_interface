#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import morse
import cubicbezier
import lj93wall

# Compute potential energy and derivative
def potenergy(pos,ends,D,alpha,r0,points,knots,clist,aD,aalpha,ar0,weps,wsigma,wallpos):
    """
    Given
        [ends[0],ends[1],ends[2]]
    -----------------------------
             |
            [-]  [pos[0],pos[1],pos[2]
               \
                \
                [-] [pos[3],pos[4],pos[5]
                 |
    ------------------------------
                [ends[3],ends[4],ends[5]]
 
    Input:
        pos             : position of the mechanophore beads
        ends            : position of the ends of the attachment beads
        D,alpha,r0      : parameters of the flanking morse potentials [morse left,morse right]
        points          : The control points stored as [points[:,0],points[:,0],\cdots,points[:,n-1]]
        knots           : Increasing ordered numbers knots[0]<knots[1]<knots[2]<\cdots<knots[n]<knots[n+1]
        clist           : end points of each cubic bezier curve [xl,xr,yl,yr] - [point.shape[1]-3,4]
        aD,aalpha,ar0   : The attachment morse potential parameters
        weps,wsigma,    : The wall potentials
        wallpos         : The wall potentials
    Output:
        energy
        derivative
    """

    bnd=np.sqrt(np.array([np.sum((pos[:3]-ends[:3])**2),np.sum((pos[:3]-pos[3:])**2),np.sum((ends[3:]-pos[3:])**2)]))
    walldist=np.absolute(np.array([pos[2]-wallpos[0],pos[2]-wallpos[1],pos[5]-wallpos[0],pos[5]-wallpos[1]]))

    #energy
    energy=0.0
    deriv=np.zeros(6)

    arr1=np.array([pos[0]-pos[3],pos[1]-pos[4],pos[2]-pos[5],pos[3]-pos[0],pos[4]-pos[1],pos[5]-pos[2]])
    drds=(1.0/bnd[1])*arr1

    #identify the energy zone of the me-me
    if (bnd[1]<=clist[0]):
        energy = energy + morse.morsepot(D[0],alpha[0],r0[0],bnd[1])
        dpotbnd = morse.dermorsepot(D[0],alpha[0],r0[0],bnd[1])
        deriv = deriv + dpotbnd*drds
    elif (bnd[1]>=clist[-1]):
        energy = energy + morse.morsepot(D[1],alpha[1],r0[1],bnd[1])
        dpotdbnd = morse.dermorsepot(D[1],alpha[1],r0[1],bnd[1])
        deriv = deriv + dpotdbnd*drds
    else:
        [knotval,p,dp,ddp]=cubicbezier.cbsyofx(points,knots,clist,bnd[1])
        energy = energy + p[1]
        dpotdbnd = (dp[1]/dp[0])
        deriv = deriv + dpotdbnd*drds

    #morse
    energy = energy + morse.morsepot(aD,aalpha,ar0,bnd[0])
    dpotdbnd = morse.dermorsepot(aD,aalpha,ar0,bnd[0])
    arr0 = np.array([pos[0]-ends[0],pos[1]-ends[1],pos[2]-ends[2],0.0,0.0,0.0])
    drds = (1.0/bnd[0])*arr0
    deriv = deriv + dpotdbnd*drds

    #morse
    energy = energy + morse.morsepot(aD,aalpha,ar0,bnd[2])
    dpotdbnd = morse.dermorsepot(aD,aalpha,ar0,bnd[2])
    arr2 = np.array([0.0,0.0,0.0,pos[3]-ends[3],pos[4]-ends[4],pos[5]-ends[5]])
    drds = (1.0/bnd[2])*arr2
    deriv = deriv + dpotdbnd*drds

    #Wall
    energy = energy + lj93wall.lj93wall(weps,wsigma,walldist[0])
    energy = energy + lj93wall.lj93wall(weps,wsigma,walldist[1])
    energy = energy + lj93wall.lj93wall(weps,wsigma,walldist[2])
    energy = energy + lj93wall.lj93wall(weps,wsigma,walldist[3])

    deriv[2]=deriv[2] + lj93wall.derlj93wall(weps,wsigma,walldist[0])*(pos[2]-wallpos[0])/walldist[0] + lj93wall.derlj93wall(weps,wsigma,walldist[1])*(pos[2]-wallpos[1])/walldist[1]
    deriv[5]=deriv[5] + lj93wall.derlj93wall(weps,wsigma,walldist[2])*(pos[5]-wallpos[0])/walldist[2] + lj93wall.derlj93wall(weps,wsigma,walldist[3])*(pos[5]-wallpos[1])/walldist[3]

    #Energy, Derivative
    return [energy,deriv]