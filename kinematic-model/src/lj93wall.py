#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import numpy as np

# LJ93 Wall potential
def lj93wall(eps,sigma,r):
    """ Return the potential due to lj93 wall
        input:
            eps   - potential depth
            sigma - length scale of the potential
            r     - distance at which the potential needs to be evaluated
        output:
            potential value
    """
    return eps*((2.0/15.0)*(sigma/r)**9-(sigma/r)**3)

# Derivative of the LJ93 Wall Potential
def derlj93wall(eps,sigma,r):
    """ Return the derivative of the potential the force due to an lj93 wall
        input:
            eps   - potential depth
            sigma - length scale of the potential
            r     - distance at which the potential needs to be evaluated
        output:
            derivative value (applied force -> gradient of potential w.r.t r)
    """
    return -3.0*eps*((2.0/5.0)*(sigma**9/(r**10))-(sigma**3/(r**4)))