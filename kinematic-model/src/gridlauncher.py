#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import potparam
import sys
import shutil
import os

if __name__ == '__main__':

    """ Code to launch a sequence of grid search jobs for different mechanophore orientations on stampede"""

    # Potential details
    [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,subsme,subsat]=potparam.paramconvfac()

    if (len(sys.argv)!=8):
        print("Usage: %s,frac,numsamples,numjobs,numsteps,totgrid,xsize,ysize"%(sys.argv[0]))
        exit()

    frac=np.double(sys.argv[1]) # Fraction of substrate LJ potential for nonbonded interactions
    numsamples=np.int(sys.argv[2]) # Number of samples of angles
    numjobs=np.int(sys.argv[3]) # Number of jobs
    numsteps=np.int(sys.argv[4]) # Number of steps to shear
    totgrid=np.int(sys.argv[5]) #Grid size
    xsize=np.int(sys.argv[6]) #number of portions on x size of grid
    ysize=np.int(sys.argv[7]) #number of portions on y size of grid

    npjobs=numsamples/numjobs #Number of angles per job

    angle=np.arcsin(2*auau[3]*np.sqrt(2.0/3.0)/dbb[0,0])
    thetaset=np.random.random(numsamples)*angle
    phiset=np.random.random(numsamples)*np.pi*2

    f=open('jobscript.sh','w')
    f.write("""#!/bin/bash
""")
    for iind in range(numjobs):
        f.write("""mkdir -p job%d\n"""%(iind+1))
        f.write("""cd job%d\n"""%(iind+1))
        f.write("""mpirun -np 8 python ../../gridsearch.py %1.2f %d %d %d %d """%(frac,numsteps,totgrid,xsize,ysize))
        for jind in range(npjobs):
            f.write("""%1.3f %1.3f """%(thetaset[iind*npjobs+jind],phiset[iind*npjobs+jind]))
        f.write('\n')
        f.write("""cd ../\n""")
    f.close()

