#!/usr/bin/env python

from mpi4py import MPI
import numpy as np
import potparam
import scipy.optimize as sco
import potential
import normpot
import sys

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

if __name__ == '__main__':

    # MPI world details
    comm=MPI.COMM_WORLD
    rank=comm.Get_rank()
    size=comm.Get_size()

    # Verify input
    inlen=len(sys.argv)
    if(inlen<=6 or (inlen-6)%2!=0):
        if(rank==0):
            print("Input: %s frac numsteps totgrid xsize ysize (ang_i phi_i)*n"%(sys.argv[0]))
        exit()

    # Frac
    frac=np.double(sys.argv[1])
    # Number of steps to shear
    numsteps=np.int(sys.argv[2])
    # Total grid size
    totgrid=np.int(sys.argv[3])
    # Xsize, Ysize
    xsize=np.int(sys.argv[4])
    ysize=np.int(sys.argv[5])
    # Get num angles
    numangles=(inlen-6)/2

    theta=np.zeros(numangles)
    phi=np.zeros(numangles)

    # For each theta get the activation behavior
    for tind in range(numangles):
        theta[tind]=np.float(sys.argv[6+2*tind])
        phi[tind]=np.float(sys.argv[6+2*tind+1])

    #Domain Decomposition
    numx=np.int(totgrid/xsize)
    numy=np.int(totgrid/ysize)

    # Potential details
    # Parameters
    
    [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,subsme,subsat]=potparam.paramconvfac(frac)

    # Normalizing
    [lscale,escale,nbnds,nr0,nD0,nalpha,npoints,nclist,nauwall,natbnd]=normpot.normparam(dbb,r0,D0,alpha,points,clist,auwall,subsat)

    # User created distances
    # Distance along z +- inter layer spacing
    nwallpos=np.array([-auau[3]*np.sqrt(2.0/3.0),auau[3]*np.sqrt(2.0/3.0)])/lscale

    # attachment alpha
    natalpha=np.linspace(2.0,10,totgrid)

    # attachment potential depth
    natD=np.linspace(0.15,2.5,totgrid)

    # Total bond length
    # ---[-]---[-]----
    ntb=nbnds[0]+2*natbnd

    # Grid start point
    count=0
    beg=np.zeros(2,dtype=np.int)
    for iind in range(xsize):
        for jind in range(ysize):
            if(rank==count):
                beg[0]=iind*numx
                beg[1]=jind*numy
            count=count+1

    # File handles for local output
    f1=open('actlog%d.txt'%(rank),'w')
    f2=open('dislog%d.txt'%(rank),'w')

    # Initializations and writing in rank0
    if(rank==0):
        np.savetxt('alphas.txt',natalpha,fmt='%2.4f')
        np.savetxt('Ds.txt',natD,fmt='%2.4f')

    # Activation flag
    act_flag=np.zeros([numx,numy],dtype=np.int)
    # Disociation flag
    dis_flag=np.zeros([numx,numy],dtype=np.int)
    # Bond length storage
    bondlengths=np.zeros(3)


    for tind in range(numangles):

        # Files - enter angles
        f1.write('angle %f %f\n\n\n\n'%(theta[tind],phi[tind]))
        f2.write('angle %f %f\n\n\n\n'%(theta[tind],phi[tind]))

        for iind in range(numx):

            aD=natD[beg[0]+iind]

            for jind in range(numy):

                alpha=natalpha[beg[1]+jind]
                # Distance to shear
                d2shear=2*ntb+max(2.0*(nbnds[1]-nbnds[0]),5.0*np.log(2.0)/alpha)
                # Velocity of shear
                velocity=(d2shear)/(2*numsteps)

                # Ends
                ends = np.array([-np.cos(theta[tind])*np.cos(phi[tind]),-np.cos(theta[tind])*np.sin(phi[tind]),-np.sin(theta[tind]),np.cos(theta[tind])*np.cos(phi[tind]),np.cos(theta[tind])*np.sin(phi[tind]),np.sin(theta[tind])])*nbnds[0]/2.0

                radius = np.sqrt(natbnd**2 - (ends[2]-nwallpos[0])**2)
                ang = np.random.rand()*np.pi*2.0
                ends[0] = ends[0] + radius*np.cos(ang)
                ends[1] = ends[1] + radius*np.sin(ang)
                ends[2] = nwallpos[0]

                radius = np.sqrt(natbnd**2 - (nwallpos[1]-ends[5])**2)
                ang = np.random.rand()*np.pi*2.0
                ends[3] = ends[3] + radius*np.cos(ang)
                ends[4] = ends[4] + radius*np.sin(ang)
                ends[5] = nwallpos[1]

                start = np.array([-np.cos(theta[tind])*np.cos(phi[tind]),-np.cos(theta[tind])*np.sin(phi[tind]),-np.sin(theta[tind]),np.cos(theta[tind])*np.cos(phi[tind]),np.cos(theta[tind])*np.sin(phi[tind]),np.sin(theta[tind])])*nbnds[0]/2.0

                # Files - enter alpha D
                f1.write('alpha %4.5f D %4.5f\n'%(alpha/lscale,aD*escale))
                f2.write('alpha %4.5f D %4.5f\n'%(alpha/lscale,aD*escale))

                # Disociation bnd_length
                disbndlength=natbnd+3.0*np.log(2)/alpha
                scale = np.min(np.log(2)/alpha/5.0,0.1)
                start=start+(np.random.rand(6)-0.5)*scale

                for kind in range(numsteps):

                    basestart = start
                    debug = False

                    ret = sco.fmin_tnc(func=potential.potenergy,x0=start,args=(ends,nD0,nalpha,nr0,npoints,knots,nclist,aD,alpha,natbnd,nauwall[0],nauwall[1],nwallpos),approx_grad=False,bounds=None,xtol=1e-10,ftol=1e-10,messages=0,maxfun=100)
                    start = ret[0]

                    if(ret[2]<0 or ret[2]>2):

                        ret = sco.minimize(potential.potenergy,x0=start,args=(ends,nD0,nalpha,nr0,npoints,knots,nclist,aD,alpha,natbnd,nauwall[0],nauwall[1],nwallpos),method='L-BFGS-B',jac=True,options={'gtol':1e-10,'maxiter': 100, 'disp': False})
                        start=ret.x

                        if(ret.success==False):

                            ret = sco.minimize(potential.potenergy,x0=start,args=(ends,nD0,nalpha,nr0,npoints,knots,nclist,aD,alpha,natbnd,nauwall[0],nauwall[1],nwallpos),method='Nelder-Mead',jac=True,options={'xtol':1E-10,'ftol':1E-10, 'maxiter': 10000, 'maxfev':10000, 'disp': False})
                            start = ret.x

                            if(ret.success==False):

                                debug=True
                                start=basestart

                    bondlengths[0]=np.sqrt(np.sum((start[:3]-ends[:3])**2))
                    bondlengths[1]=np.sqrt(np.sum((start[:3]-start[3:])**2))
                    bondlengths[2]=np.sqrt(np.sum((ends[3:]-start[3:])**2))

                    if(bondlengths[1]>=nbnds[2]):
                        act_flag[iind,jind]=act_flag[iind,jind]+1
                        f1.write('%d \n'%(act_flag[iind,jind]))
                        f2.write('%d \n'%(dis_flag[iind,jind]))
                        break

                    if(bondlengths[0]>disbndlength or bondlengths[2]>disbndlength):
                        dis_flag[iind,jind]=dis_flag[iind,jind]+1
                        f1.write('%d \n'%(act_flag[iind,jind]))
                        f2.write('%d \n'%(dis_flag[iind,jind]))
                        break

                    ends[0]=ends[0]-velocity
                    ends[3]=ends[3]+velocity

                f1.write('\n')
                f2.write('\n')

            f1.write('\n\n')
            f2.write('\n\n')

        f1.write('\n\n\n')
        f2.write('\n\n\n')

        f1.flush()
        f2.flush()

    f1.close()
    f2.close()

    np.savetxt('actflag%d.txt'%(rank),act_flag,fmt='%d',header='%d %d'%(beg[0],beg[1]))
    np.savetxt('disflag%d.txt'%(rank),dis_flag,fmt='%d',header='%d %d'%(beg[0],beg[1]))
