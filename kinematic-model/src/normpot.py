#!/usr/bin/env python
import numpy as np

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

# Function to normalize the parameters
def normparam(dbb,r0,D0,alpha,points,clist,auwall,subsat):
    """ Function to normalize values based on the first minima of the double well
    Input:
        dbb - double well extrema points
        r0,D0,alpha - double well flanking morse potentials
        points - double well control points
        clist - curve boundary points in x axis
        auwall - the wall parameters
        subsat - the attachment parameters
    Output:
        lscale - length scale
        escale - energy scale
            Normalized quantities
        nbnds - bonds in the double well [0,1,2] - 1st minima,2nd minima, maxima
        nr0,nD0,nalpha - flanking double well
        nclist - the curve end points
        nauwall - the wall parameters
        natbnd - the attachment bond
    """

    # Normalizing
    lscale=dbb[0,0]
    escale=-dbb[1,0]

    # Dblwell positions
    nbnds=np.copy(dbb[0,0:])/lscale

    # Dblwell flanking morse potentials
    nr0=r0/lscale
    nD0=D0/escale
    nalpha=alpha*lscale

    # Dblwell control points
    npoints=np.copy(points)
    npoints[0,:]=npoints[0,:]/lscale
    npoints[1,:]=npoints[1,:]/escale

     # clist - end points of polynomials
    nclist=np.copy(clist)
    nclist=nclist/lscale

    # Wall potential
    nauwall=np.copy(auwall)
    nauwall[0]=nauwall[0]/escale
    nauwall[1:]=nauwall[1:]/lscale

    # Attachment potential
    natbnd=subsat/lscale

    return [lscale,escale,nbnds,nr0,nD0,nalpha,npoints,nclist,nauwall,natbnd]
