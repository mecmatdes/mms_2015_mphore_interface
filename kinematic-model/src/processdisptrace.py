#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of kinematic-model package that solves the kinematic model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# kinematic-model is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# kinematic-model is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import glob
import numpy as np

def addarrays(arr1,arr2):
    """Add two arrays with NaNs in their elements. If there are NaNs we ignore them and add the rest of the values
    Input:
    arr1, arr2 -- two arrays of equal size
    Output:
    val -- sum of these arrays ignoring the NaNs
    """
    val=np.zeros(arr1.shape)

    truth1=np.isnan(arr1)
    truth2=np.isnan(arr2)

    ctruth=np.logical_and(truth1,truth2)
    val[ctruth]=arr1[ctruth]+arr2[ctruth]

    ctruth=np.logical_and(np.logical_not(truth1),np.logical_not(truth2))
    val[ctruth]=arr1[ctruth]+arr2[ctruth]

    ctruth=np.logical_and(np.logical_not(truth1),truth2)
    val[ctruth]=arr1[ctruth]

    ctruth=np.logical_and(truth1,np.logical_not(truth2))
    val[ctruth]=arr2[ctruth]

    return val

if __name__ == '__main__':

    # List of all actflag files
    actlist=glob.glob('actflag[0-9]*.txt')
    actflag=np.loadtxt(actlist[0])
    
    # List of all disflags files
    dislist=glob.glob('disflag[0-9]*.txt')
    disflag=np.loadtxt(dislist[0])

    # List of all cosines files
    coslist=glob.glob('cossqphi[0-9]*.txt')
    cossqphi=np.loadtxt(coslist[0])

    # List of all files containing the cosines of the activated mechanophores
    actcoslist=glob.glob('actcossqphi[0-9]*.txt')
    actcossqphi=np.loadtxt(actcoslist[0])
    presentlist=np.logical_not(np.isnan(actcossqphi))*1.0

    # Sums of actflags
    for fil in range(len(actlist)-1):
        actflag=actflag+np.loadtxt(actlist[fil+1])

    # Sums of disflags
    for fil in range(len(dislist)-1):
        disflag=disflag+np.loadtxt(dislist[fil+1])

    # Sums of cosines
    for fil in range(len(coslist)-1):
        cossqphi=cossqphi+np.loadtxt(coslist[fil+1])

    # Sums of all activated cosines
    for fil in range(len(actcoslist)-1):
        tmp=np.loadtxt(actcoslist[fil+1])
        presentlist=presentlist+np.logical_not(np.isnan(tmp))*1.0
        actcossqphi=addarrays(tmp,actcossqphi)

    np.savetxt('actflag.txt',actflag,fmt='%d')
    np.savetxt('disflag.txt',disflag,fmt='%d')
    np.savetxt('cossqphi.txt',(3.0*cossqphi/(1.0*len(coslist))-1.0)/2.0,fmt='%4.4f')
    np.savetxt('actcossqphi.txt',(3.0*actcossqphi/(1.0*presentlist)-1.0)/2.0,fmt='%4.4f')
