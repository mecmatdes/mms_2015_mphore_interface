#!/usr/bin/env python

import numpy as np
import random

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of "substrate" package that builds the substrate for the md-model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# substrate is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# substrate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

def paramconvfac():
    """ Parameters and conversion factors for the substrate
    """
    
    # L J Parameters for auau and the walls
    #eps0 sigma cutoff r0
    auau=np.zeros(4)
    auau[0]=12.306 #kcal/mol #eps0
    auau[3]=2.8847 #Angstroms #r0
    auau[1]=auau[3]/(2.0**(1.0/6.0)) #Angstroms #sigma
    auau[2]=2.5*auau[3] #cutoff

    num_density=4.0/((auau[3]*np.sqrt(2))**3)

    #eps0 sigma cutoff walldist
    auwall=np.zeros(4)
    auwall[0]=(2.0/3.0)*np.pi*num_density*(auau[1]**3)*auau[0] #eps0
    auwall[1]=auau[1] #sigma
    auwall[2]=auau[2] #cutoff
    auwall[3]=auwall[1]/(2.5**(1.0/6.0))

    # Mass of gold
    mg=196.96657 #amu #g/mole
    #useful constants
    kcal=4184.0 #joule
    avagadro=6.0221413E23
    ev=1.60217657E-19 #Joule
    boltzmann=1.3806488E-23 #Joule/Kelvin
    atm=1.01325E5 #Pa

    #conversion parameters
    conv=np.zeros(7)
    conv[0]=mg #1 unit of mass is conv[0] mass of gold in amu
    conv[1]=auau[1] #1 unit of distance is conv[1] Angs
    conv[2]=auau[0] #1 unit of energy in kcal/mol
    conv[3]=auau[0]*kcal/avagadro/boltzmann # 1 unit of temperature in K
    conv[4]=np.sqrt(mg*(auau[1]**2)*1E-23/(auau[0]*kcal))*1E15 #1 unit of time in fs
    conv[5]=((conv[2]*kcal/avagadro)/(conv[1]*1E-10))*1E9 #1 unit of force in nN
    conv[6]=(conv[2]*kcal/avagadro)/(conv[1]**3*1E-30)/atm #1 unit of pressure in atm

    return [auau,auwall,num_density,conv]

