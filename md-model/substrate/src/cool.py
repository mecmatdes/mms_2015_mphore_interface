#!/usr/bin/env python

import numpy as np
import potentialdata as pd

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of "substrate" package that builds the substrate for the md-model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# substrate is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# substrate is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

# collect potential info, conversion factors
[auau,auwall,num_density,conv]=pd.paramconvfac()

# Simulation details
# Steps over which cooling happens
numsteps=10
# Temperature values for these steps from 300 K to 1000 K in LJ Units
temp=np.linspace(300,1000,numsteps)/conv[3]
# Timestep 1 fs converted to LJ units
timestep=1.0000/conv[4]
# Damp factor unit of time for Noose Hoover dynamics
damp=np.array([1000,100])*timestep
# Number of steps involved in transitioning and equilibration at each temperature
steps=np.array([10000,40000]) 
# Pressure of 1 atm converted to LJ units
press=1.0/conv[6] 
# Simulation box dimensions
    # scale factors indicating length per layer along each direction 
scale=(auau[3]/conv[1])*np.array([1.0,np.sqrt(1/3),np.sqrt(2/3)])
    # box dimensions, adjustment so as to not overlap the periodic particles and adjustment for the wall
box=np.array([[0,27.9999*scale[0]],[0,47.9999*scale[1]],[0,6.00*scale[2]+auwall[3]/conv[1]]])
    # Fill box dimension, basically less the wall height
fill=np.array([[0,27.9999*scale[0]],[0,47.9999*scale[1]],[0,6.00*scale[2]]])

#Write Lammps Simulation File
f=open('in.lammps','w')
f.write("""# Initialization
clear

# Choice of units - real units
units lj

# 3 dimensional problem
dimension 3

# We are simulating the interface
# so it is periodic on two sides and not on the third side
boundary p p s

# Type of atoms involved
atom_style charge

# Mapping style
atom_modify map array

# Create regions
region sim block %f %f %f %f %f %f
region fill block %f %f %f %f %f %f

"""%(box[0,0],box[0,1],box[1,0],box[1,1],box[2,0],box[2,1],fill[0,0],fill[0,1],fill[1,0],fill[1,1],fill[2,0],fill[2,1]))

f.write("""
# Create simulation box
create_box 1 sim

# Fill up atoms
lattice fcc %f orient x 1 0 -1 orient y -1 2 -1 orient z 1 1 1 origin 0 0 0
create_atoms 1 region fill

# Set atom prop
mass 1 1

# Neighbor set up
neighbor 1.0 bin
neigh_modify delay 1 check yes

thermo 1
thermo_style custom step temp pe ke lx ly lz pxx pyy
thermo_modify norm no

# Force Fields
pair_style lj/cut %f
pair_coeff 1 1 1 1

# Fix wall
fix wallhi all wall/lj93 zhi EDGE %f %f %f

dump 101 all custom 100 config-*.dat type x y z id element

# Minimize
min_style fire
minimize 1.0e-14 1.0e-16 1000 10000

undump 101

"""%(1.0,auau[2]/conv[1],auwall[0]/conv[2],auwall[1]/conv[1],auwall[2]/conv[1]))

# Equilibration at High Temperature of 1000K
f.write('#Equilibrate\n')
f.write('timestep %f\n'%(timestep))
f.write('velocity all create %f 467876\n\n'%(temp[-1]))
f.write('dump 101 all custom 1000 config-*.dat type x y z id element\n\n')
f.write('fix 1 all npt temp %f %f %f x %f %f %f y %f %f %f\n'%(temp[-1],temp[-1],damp[0],press,press,damp[0],press,press,damp[0]))
f.write('run %d\n'%(steps[0]))
f.write('unfix 1\n\n')

for ind in range(numsteps-1):
    # Equilibration step
    f.write('timestep %f\n'%(timestep))
    f.write('fix 1 all npt temp %f %f %f x %f %f %f y %f %f %f\n'%(temp[-ind-1],temp[-ind-1],damp[1],press,press,damp[1],press,press,damp[1]))
    f.write('run %d\n'%(steps[1]))
    f.write('unfix 1\n\n')

    f.write('write_restart restart%d.equil\n\n'%(ind))
    
    # Transition step
    f.write('timestep %f\n'%(timestep))
    f.write('fix 1 all npt temp %f %f %f x %f %f %f y %f %f %f\n'%(temp[-ind-1],temp[-ind-2],damp[0],press,press,damp[0],press,press,damp[0]))
    f.write('run %d\n'%(steps[0]))
    f.write('unfix 1\n\n')

# Final Equilibration
f.write('timestep %f\n'%(timestep))
f.write('fix 1 all npt temp %f %f %f x %f %f %f y %f %f %f\n'%(temp[-numsteps],temp[-numsteps],damp[1],press,press,damp[1],press,press,damp[1]))
f.write('run %d\n'%(steps[1]))
f.write('unfix 1\n\n')

f.write('write_restart restart%d.equil\n\n'%(numsteps-1))

f.write('undump 101\n\n')
f.write('unfix wallhi\n')

f.close()
