#!/usr/bin/env python

import numpy as np

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of "quasiequilibriumshear" package that builds the substrate for the md-model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# quasiequilibriumshear is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# quasiequilibriumshear is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

# Arrange Substrate Atoms
def arrsubsatoms(configfil1,configfil2,scale,shift):
    """
    Arrange substrate atoms one on top of the other
    Input:
        configfil1 - configuration file 1
        configfil2 - configuration file 2
        scale      - length scale, used to scale the configuration
        shift      -
            shift[0] - shift the lower substrate
            shift[1] - dist between the two substrates
            shift[2] - shift the upper substrate
    Output:
        atomset - atom coordinates in x y z
        bounds  - [[0 xhi]
                   [0 yhi]
                   [0 zhi]]
        natm    - [natm[0]  number of atoms in bottom substrate
                - natm[1]]  number of atoms in top substrate
        """

    # Load number of atoms
    # Load box size
    natm=np.zeros(2,dtype=np.int)
    box=np.zeros([3,4])

    f1=open(configfil1,'r')
    f1.readline()
    f1.readline()
    f1.readline()
    natm[0]=np.int(f1.readline())
    f1.readline()
    line=f1.readline().split()
    box[0,0]=np.float(line[0])*scale
    box[0,1]=np.float(line[1])*scale
    line=f1.readline().split()
    box[1,0]=np.float(line[0])*scale
    box[1,1]=np.float(line[1])*scale
    line=f1.readline().split()
    box[2,0]=np.float(line[0])*scale
    box[2,1]=np.float(line[1])*scale
    f1.close()

    f2=open(configfil2,'r')
    f2.readline()
    f2.readline()
    f2.readline()
    natm[1]=np.int(f2.readline())
    f2.readline()
    line=f2.readline().split()
    box[0,2]=np.float(line[0])*scale
    box[0,3]=np.float(line[1])*scale
    line=f2.readline().split()
    box[1,2]=np.float(line[0])*scale
    box[1,3]=np.float(line[1])*scale
    line=f2.readline().split()
    box[2,2]=np.float(line[0])*scale
    box[2,3]=np.float(line[1])*scale
    f2.close()

    # Load content of the files
    config1=np.loadtxt(configfil1,usecols=[0,1,2,3],skiprows=9,ndmin=2)
    config2=np.loadtxt(configfil2,usecols=[0,1,2,3],skiprows=9,ndmin=2)

    config1[:,1:4]=config1[:,1:4]*scale
    config2[:,1:4]=config2[:,1:4]*scale

    # First substrate from bottom up
    # Second substrate from top down

    # Orient every atom from the origin of its respective box
    config1[:,1]=config1[:,1]-box[0,0]
    config1[:,2]=config1[:,2]-box[1,0]
    config1[:,3]=box[2,1]-config1[:,3]

    config2[:,1]=config2[:,1]-box[0,2]
    config2[:,2]=config2[:,2]-box[1,2]
    config2[:,3]=box[2,3]-config2[:,3]

    # Arrange the boxes
    # Compute the bounds
    bounds=np.zeros([3,2])
    bounds[0,1]=max(box[0,1]-box[0,0],box[0,3]-box[0,2])
    bounds[1,1]=max(box[1,1]-box[1,0],box[1,3]-box[1,2])
    bounds[2,1]=box[2,3]-box[2,2]+box[2,1]-box[2,0]+shift[0]+shift[1]+shift[2]
                                                    #Wall Separation Wall

    atomset=np.zeros([natm[0]+natm[1],3])

    # atomset
    # x y z
    # bottom half
    for iind in range(natm[0]):
        atomset[iind,:]=config1[iind,1:4]+np.array([0,0,shift[0]])
                                            # Wall height
    #top half
    # x y z
    for iind in range(natm[1]):
        atomset[natm[0]+iind,0:2]=config2[iind,1:3]
        atomset[natm[0]+iind,2]=bounds[2,1]-config2[iind,3]-shift[2]
                                # Place from above #Wall height
    return [atomset,bounds,natm]

def randplacemphore(nsims,nmphpsim,overest,bounds,shift,mpbnds,atomset,natm,subsat):
    """
    Randomly place mechanophores

    Input:
        nsims   - number of simulations
        nmphsim - number of mechanophores per simulations
        overest - over estimate the number of mechanophores
        bounds  - bounds of the simulation box
        shift   -
            shift[0] - shift the lower substrate
            shift[1] - dist between the two substrates
            shift[2] - shift the upper substrate
        mpbnds  - mechanophore stationary points
        atomset - substrate atoms
        natm    - [natm[0]  number of atoms in bottom substrate
                   natm[1]]  number of atoms in top substrate
        subsat  - substrate attachment potentials

    Output:
        mphoresets  - mphoresets [[mphorenum_1 mphorenum_2 ... mphorenum_nmphsim][] ... []]
        subconnect  - [[bottom subs atm1, top subs atm1][]...[]]
        bset        - [[x y z][]...[]]
        tset        - [[x y z][]...[]]
    """

    allmphores=nsims*nmphpsim*overest

    #define margin
    margin=2*mpbnds[0]
    rat=np.array([margin/bounds[0,1],margin/bounds[1,1]])
    #internal region
    invrat=1.0-rat

    #Allocate
    mset=np.zeros([allmphores,3])
    bset=np.zeros([allmphores,3])
    tset=np.zeros([allmphores,3])
    subconnect=np.zeros([allmphores,2],dtype=np.int)
    centroid=np.zeros([allmphores,3])
    far=np.zeros([allmphores,allmphores],dtype=np.bool)

    #Random set of points four times the normal set
    mset[:,0:2]=np.random.rand(allmphores,2)*invrat+rat/2.0
    mset[:,0]=mset[:,0]*bounds[0,1]
    mset[:,1]=mset[:,1]*bounds[1,1]
    mset[:,2]=0.5*(bounds[2,0]+bounds[2,1])

    # Phi Theta
    Phi=np.random.rand(allmphores)*2*np.pi
    maxth=np.arcsin(shift[1]/mpbnds[0])
    Theta=np.random.rand(allmphores)*maxth

    # Top set
    tset[:,0]=mset[:,0]+mpbnds[0]*np.cos(Theta)*np.cos(Phi)/2.0
    tset[:,1]=mset[:,1]+mpbnds[0]*np.cos(Theta)*np.sin(Phi)/2.0
    tset[:,2]=mset[:,2]+mpbnds[0]*np.sin(Theta)/2.0

    # Bottom set
    bset[:,0]=mset[:,0]-mpbnds[0]*np.cos(Theta)*np.cos(Phi)/2.0
    bset[:,1]=mset[:,1]-mpbnds[0]*np.cos(Theta)*np.sin(Phi)/2.0
    bset[:,2]=mset[:,2]-mpbnds[0]*np.sin(Theta)/2.0

    #For each point find a substrate atom to work with and orient the other mechanophore bead randomly
    baseloc=range(0,natm[0])
    toploc=range(natm[0],natm[1]+natm[0])

    lowlim=0
    #lowlim=subsat[2]-np.log(2)/subsat[1]
    highlim=subsat[2]+0.5*np.log(2)/subsat[1]
    for iind in range(allmphores):

        dist1=np.sqrt(np.sum((bset[iind,:]-atomset[baseloc,:])**2,1))
        loc1=np.where(np.logical_and(dist1>lowlim,dist1<highlim))[0]
        dist2=np.sqrt(np.sum((tset[iind,:]-atomset[toploc,:])**2,1))
        loc2=np.where(np.logical_and(dist2>lowlim,dist2<highlim))[0]

        if(len(loc1)==0 or len(loc2)==0):

            flag=True

            while(flag):

                mset[iind,0:2]=np.random.rand(2)*invrat+rat/2.0
                mset[iind,0]=mset[iind,0]*bounds[0,1]
                mset[iind,1]=mset[iind,1]*bounds[1,1]

                th=np.random.rand(1)*maxth
                ph=np.random.rand(1)*2.0*np.pi

                tset[iind,0]=mset[iind,0]+mpbnds[0]*np.cos(th)*np.cos(ph)/2.0
                tset[iind,1]=mset[iind,0]+mpbnds[0]*np.cos(th)*np.sin(ph)/2.0
                tset[iind,2]=mset[iind,0]+mpbnds[0]*np.sin(th)/2.0

                bset[iind,0]=mset[iind,0]-mpbnds[0]*np.cos(th)*np.cos(ph)/2.0
                bset[iind,1]=mset[iind,0]-mpbnds[0]*np.cos(th)*np.sin(ph)/2.0
                bset[iind,2]=mset[iind,0]-mpbnds[0]*np.sin(th)/2.0

                dist1=np.sqrt(np.sum((bset[iind,:]-atomset[baseloc,:])**2,1))
                loc1=np.where(np.logical_and(dist1>lowlim,dist1<highlim))[0]

                dist2=np.sqrt(np.sum((tset[iind,:]-atomset[toploc,:])**2,1))
                loc2=np.where(np.logical_and(dist2>lowlim,dist2<highlim))[0]

                if(len(loc1)!=0 and len(loc2)!=0):
                    flag=False

        subconnect[iind,0]=baseloc[loc1[np.argmin(dist1[loc1])]]
        subconnect[iind,1]=toploc[loc2[np.argmin(dist2[loc2])]]

    distsep=2*mpbnds[0]
    for iind in range(allmphores):
        far[iind,:]=(np.sum((mset[iind,:]-mset)**2,1))>(distsep**2)

    #find sets of eight non interacting pairs of mechanophores
    mphoresets=np.zeros([nsims,nmphpsim],dtype=np.int)
    availlist=list(range(allmphores))
    for iind in range(nsims):
        flag=True
        mphore=[]
        while(flag):
            copylist=list(availlist)
            mphore=[]
            for jind in range(nmphpsim-1):
                mphore.append(np.random.choice(copylist))
                copylist=list(set(copylist).intersection(np.where(far[mphore[-1],:])[0]))
                if(len(copylist)==0):
                    break
            if(len(copylist)!=0):
                mphore.append(np.random.choice(copylist))
                flag=False

        for jind in range(nmphpsim):
            mphoresets[iind,jind]=mphore[jind]
            availlist.remove(mphore[jind])

    return [mphoresets,subconnect,bset,tset]
