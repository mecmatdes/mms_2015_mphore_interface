#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of "quasiequilibriumshear" package that builds the substrate for the md-model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# quasiequilibriumshear is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# quasiequilibriumshear is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.


import sys,os,shutil
import numpy as np

import potparam
import arrint

#import matplotlib
#matplotlib.use('Agg')
#from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.pyplot as plt
#matplotlib.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica'],'size':32})
#matplotlib.rc('text', usetex=True)


if __name__ == '__main__':

    if(len(sys.argv)<3):
        print("ERROR: USAGE %s config1fil config2fil"%(sys.argv[0]))
        exit()

    # Potential parameters + conversion factors #Write potential
    frac=0.10
    [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,auin,subsme,subsat,breakval]=potparam.param(frac)
    conv=potparam.convparam()
    numsamp=potparam.writedoublewell(201,'pot.dat')

    # Read files and store the atoms
    shift=np.array([auwall[3],2*auau[3]*np.sqrt(2.0/3.0),auwall[3]])
    [atomset,bounds,natm]=arrint.arrsubsatoms(sys.argv[1],sys.argv[2],conv[1],shift)

    # Number of simulations
    nsims=25
    # Number of mechanophores per simulation
    nmphpsim=8
    # The lengths associated with mechanophore
    mpbnds=np.copy(dbb[0,:])
    # Overstimate mechanophores to choose from
    overest=2
    # Randomly assign mphores
    [mphoresets,subconnect,bset,tset]=arrint.randplacemphore(nsims,nmphpsim,overest,bounds,shift,mpbnds,atomset,natm,subsat)

    # plt.figure(1,figsize=(24,12))
    # plt.subplot(121)
    # plt.plot(atomset[:natm[0],0],atomset[:natm[0],2],'or')
    # plt.plot(atomset[natm[0]:,0],atomset[natm[0]:,2],'og')
    # plt.plot(bset[:,0],bset[:,2],'oy',alpha=0.25)
    # plt.plot(tset[:,0],tset[:,2],'om',alpha=0.25)
    # plt.plot([bounds[0,0],bounds[0,1],bounds[0,1],bounds[0,0],bounds[0,0]],[bounds[2,0],bounds[2,0],bounds[2,1],bounds[2,1],bounds[2,0]],'-b',linewidth=2)
    # plt.subplot(122)
    # plt.plot(bset[:,0],bset[:,1],'oy',alpha=0.25)
    # plt.plot(tset[:,0],tset[:,1],'om',alpha=0.25)
    # plt.plot([bounds[0,0],bounds[0,1],bounds[0,1],bounds[0,0],bounds[0,0]],[bounds[1,0],bounds[1,0],bounds[1,1],bounds[1,1],bounds[1,0]],'-b',linewidth=2)
    # plt.tight_layout()
    # plt.savefig('config.png',dpi=128,format='png')

    # Number of atom types
    nattypes=4
    # Number of atoms
    numatoms=natm[0]+natm[1]+nmphpsim*2
    # Number of bond types
    nbndtypes=3
    nbnds=nmphpsim*3
    # Mass
    mass=np.array([conv[0],conv[0],conv[0],conv[0]])
    # Timestep
    timestep=1 #E-15
    # Damp time for equilibration after pe minimization
    damp=timestep*np.array([100])
    # Steps
    # Equilibration Shear
    nsteps=np.array([40000,40,10000])
    # Frames
    nframes=1000
    temp=300 #K

    # atoms, bonds, atom types, bond types
    f=open('interface.dat','w')
    f.write('This file contains the position of the atoms an mphores for the interface\n')
    f.write('%d atoms\n'%(numatoms))
    f.write('%d bonds\n'%(nbnds))
    f.write('%d atom types\n'%(nattypes))
    f.write('%d bond types\n'%(nbndtypes))
    f.write('%f %f xlo xhi\n'%(0,bounds[0,1]/conv[1]))
    f.write('%f %f ylo yhi\n'%(0,bounds[1,1]/conv[1]))
    f.write('%f %f zlo zhi\n'%(0,bounds[2,1]/conv[1]))

    f.write('\n')
    f.write('Masses\n')
    f.write('\n')
    for jind in range(nattypes):
        f.write('%d %f\n'%(jind+1,mass[jind]/conv[0]))
    f.write('\n')

    f.write('Atoms\n')
    f.write('\n')
    # Storage Order
    # id moleculeid type x y z
    molid=1
    attype=1

    for jind in range(natm[0]):
        f.write('%d %d %d %f %f %f\n'%(jind+1,molid,attype,atomset[jind,0]/conv[1],atomset[jind,1]/conv[1],atomset[jind,2]/conv[1]))

    molid=2
    attype=4

    for jind in range(natm[0],natm[0]+natm[1]):
        f.write('%d %d %d %f %f %f\n'%(jind+1,molid,attype,atomset[jind,0]/conv[1],atomset[jind,1]/conv[1],atomset[jind,2]/conv[1]))

    f.close()
#
    f=open('in.lammps','w')
    f.write("""# Initialization
clear

# Choice of units - relative units
units lj

# 3 dimensional problem
dimension 3

# We are simulating the interface
# so it is periodic on two sides and not on the third side
boundary p p f

# Type of atoms involved
atom_style bond

# Mapping style
atom_modify map array

# Read Data
read_data interface.dat

# Neighbor set up
neighbor 1.0 bin
neigh_modify delay 1 check yes
comm_modify mode single cutoff %f vel yes
#communicate single cutoff -- vel yes

# Time Step
reset_timestep 0
timestep %f

"""%(2*np.max(breakval)/conv[1],timestep/conv[4]))

    # Note: Minimal interaction between substrates
    f.write("""# Force Fields
pair_style lj/cut %f
pair_coeff 1 1 %f %f
pair_coeff 1 4 %f %f %f
pair_coeff 4 4 %f %f
"""%(auau[2]/conv[1],auau[0]/conv[2],auau[1]/conv[1],auin[0]/conv[2],auin[1]/conv[1],auin[2]/conv[1],auau[0]/conv[2],auau[1]/conv[1]))

    f.write("""pair_coeff 1 2 %f %f %f
pair_coeff 1 3 %f %f %f
pair_coeff 2 4 %f %f %f
pair_coeff 3 4 %f %f %f
"""%(subsme[0]/conv[2],subsme[1]/conv[1],subsme[2]/conv[1],subsme[0]/conv[2],subsme[1]/conv[1],subsme[2]/conv[1],subsme[0]/conv[2],subsme[1]/conv[1],subsme[2]/conv[1],subsme[0]/conv[2],subsme[1]/conv[1],subsme[2]/conv[1]))

    f.write("""pair_coeff 2 3 %f %f %f
pair_coeff 2 2 %f %f %f
pair_coeff 3 3 %f %f %f
bond_style hybrid morse table linear %d
bond_coeff 2 table pot.dat DOUBLE # Double well
bond_coeff 1 morse %f %f %f # Attachment
bond_coeff 3 morse %f %f %f # Attachment
"""%(0.0001,0.0001,auau[2]/conv[1],0.0001,0.00001,auau[2]/conv[1],0.0001,0.00001,auau[2]/conv[1],numsamp,subsat[0]/conv[2],subsat[1]*conv[1],subsat[2]/conv[1],subsat[0]/conv[2],subsat[1]*conv[1],subsat[2]/conv[1]))

    f.write("""# Create the walls
fix wallhilo all wall/lj93 zlo EDGE %f %f %f zhi EDGE %f %f %f
fix_modify wallhilo energy yes

# Form groups of atoms
group subs1 molecule %d
group subs2 molecule %d
group subs union subs1 subs2
group mp subtract all subs

"""%(auwall[0]/conv[2],auwall[1]/conv[1],auwall[2]/conv[1],auwall[0]/conv[2],auwall[1]/conv[1],auwall[2]/conv[1],1,2))

    f.close()
    # rigid moving zones heights
    levels=np.array([bounds[2,0]+auwall[3]+auau[3]*np.sqrt(2/3),bounds[2,1]-auwall[3]-auau[3]*np.sqrt(2/3),(bounds[2,0]+bounds[2,1])/2.0-2*auau[3]*np.sqrt(2/3),(bounds[2,0]+bounds[2,1])/2.0+2*auau[3]*np.sqrt(2/3)])

    dist2move=2*(dbb[0,0]+2*subsat[2])+(dbb[0,1]-dbb[0,0])
    disppstep=dist2move/(2*nsteps[1])

    for iind in range(nsims):
        os.makedirs("config%d"%(iind),exist_ok=True)
        shutil.copyfile("interface.dat","./config%d/interface.dat"%(iind))
        shutil.copyfile("in.lammps","./config%d/in.lammps"%(iind))
        shutil.copyfile("pot.dat","./config%d/pot.dat"%(iind))

        f=open('config%d/interface.dat'%(iind),'a')
        molid=2
        #add the mechanophore atoms
        #Storage Order
        #id moleculeid type x y z
        atmcounter=natm[0]+natm[1]
        for jind in range(nmphpsim):
            molid=molid+1
            whichmphore=mphoresets[iind,jind]
            atmcounter=atmcounter+1
            f.write('%d %d %d %f %f %f\n'%(atmcounter,molid,2,bset[whichmphore,0]/conv[1],bset[whichmphore,1]/conv[1],bset[whichmphore,2]/conv[1]))
            atmcounter=atmcounter+1
            f.write('%d %d %d %f %f %f\n'%(atmcounter,molid,3,tset[whichmphore,0]/conv[1],tset[whichmphore,1]/conv[1],tset[whichmphore,2]/conv[1]))

        # prescribe the bonds
        f.write('\n')
        f.write('Bonds\n')
        f.write('\n')

        #Storage Order
        #id type atom1 atom2
        bndcounter=0
        atmcounter=natm[0]+natm[1]
        atlist=[]
        for jind in range(nmphpsim):
            whichmphore=mphoresets[iind,jind]
            atlist.append(subconnect[whichmphore,0]+1)
            atlist.append(subconnect[whichmphore,1]+1)
            bndcounter=bndcounter+1
            atmcounter=atmcounter+1
            f.write('%d %d %d %d\n'%(bndcounter,1,atmcounter,subconnect[whichmphore,0]+1))
            bndcounter=bndcounter+1
            f.write('%d %d %d %d\n'%(bndcounter,2,atmcounter,atmcounter+1)) # Mphore bond
            atmcounter=atmcounter+1
            bndcounter=bndcounter+1
            f.write('%d %d %d %d\n'%(bndcounter,3,atmcounter,subconnect[whichmphore,1]+1))
        f.close()

        f=open('config%d/in.lammps'%(iind),'a')
        f.write('group subat id ')
        for jind in range(nmphpsim):
            f.write('%d %d '%(atlist[2*jind],atlist[2*jind+1]))
        f.write('\n')
        f.write('group mpat union mp subat\n')

        f.write("""# Shear
# Create the top and bottom regions
region botwall block INF INF INF INF INF %f
region topwall block INF INF INF INF %f INF

# Interface
region mid block INF INF INF INF %f %f
# Either end of the Interface
region bot block INF INF INF INF %f %f
region top block INF INF INF INF %f %f

# Form the respective groups
group topwallg region topwall
group botwallg region botwall
group restg subtract all topwallg botwallg
group midg region mid
group botg region bot
group topg region top

"""%(levels[0]/conv[1],levels[1]/conv[1],levels[2]/conv[1],levels[3]/conv[1],levels[0]/conv[1],levels[2]/conv[1],levels[3]/conv[1],levels[1]/conv[1]))

        f.write("""#Compute
compute baat mpat property/local batom1 batom2 btype
compute bdfe mpat bond/local dist force eng

thermo 1
thermo_style custom step pe ke temp
thermo_modify norm no

# Minimize Potential Energy
min_style fire
minimize 1.0e-14 1.0e-16 1000 10000


""")

        f.write("""# Equilibrate
velocity subs create %f 2313
#restart 10000 interface.restart
dump 101 all custom %d interface.conf.dat id type xu yu zu
dump 102 mpat local 1 interface.mphore.dat c_baat[1] c_baat[2] c_baat[3] c_bdfe[1] c_bdfe[2] c_bdfe[3]
fix 2 all nvt temp %f %f %f tloop 10
run %d
unfix 2
undump 102
undump 101


"""%(temp/conv[3],nframes,temp/conv[3],temp/conv[3],damp/conv[4],nsteps[0]))


        f.write(""" #Computes
compute midspa midg stress/atom NULL
compute midavg midg reduce sum c_midspa[1] c_midspa[2] c_midspa[3] c_midspa[4] c_midspa[5] c_midspa[6]

compute topf midg group/group topg
compute botf midg group/group botg

compute kei restg ke
compute pepai restg pe/atom
compute pei restg reduce sum c_pepai
compute tempi restg temp
compute ppai restg stress/atom NULL
compute pressi restg reduce sum c_ppai[1] c_ppai[2] c_ppai[3] c_ppai[4] c_ppai[5] c_ppai[6]

# Displays
thermo 1
thermo_style custom step c_pei c_kei c_tempi c_pressi[1] c_pressi[2] c_pressi[3] c_pressi[4] c_pressi[5] c_pressi[6] c_midavg[1] c_midavg[2] c_midavg[3] c_midavg[4] c_midavg[5] c_midavg[6] c_topf[1] c_topf[2] c_topf[3] c_botf[1] c_botf[2] c_botf[3]
thermo_modify norm no

""")
        f.write("""dump 101 mpat custom %d shear.mphoreconf.dat id type xu yu zu
dump 102 mpat local 1 shear.mphore.dat c_baat[1] c_baat[2] c_baat[3] c_bdfe[1] c_bdfe[2] c_bdfe[3]
variable loopvar loop %d
label loop

displace_atoms topwallg move %f 0.0 0.0 units box
displace_atoms botwallg move -%f 0.0 0.0 units box

fix 2 restg nvt temp %f %f %f tloop 10
fix 3 mpat bond/break %d 1 %f
fix 4 mpat bond/break %d 2 %f # Double well
fix 5 mpat bond/break %d 3 %f

#fix 6 topwallg setforce 0.0 0.0 0.0
#fix 7 botwallg setforce 0.0 0.0 0.0
#min_style cg
#minimize 1.0e-6 1.0e-8 100 1000
#unfix 7
#unfix 6

run %d

unfix 5
unfix 4
unfix 3
unfix 2

next loopvar
jump in.lammps loop
undump 101
undump 102
unfix wallhilo
"""%(nframes,nsteps[1],disppstep/conv[1],disppstep/conv[1],temp/conv[3],temp/conv[3],damp/conv[4],1,breakval[1]/conv[1],1,breakval[0]/conv[1],1,breakval[1]/conv[1],nsteps[2]))
        f.close()

