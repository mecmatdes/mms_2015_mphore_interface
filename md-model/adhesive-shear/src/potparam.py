#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of "adhesiveshear" package that builds the substrate for the md-model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# adhesiveshear is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# adhesiveshear is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import cubicbezier
import morse
import scipy.optimize as sco

# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt
# matplotlib.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica'],'size':32})
# matplotlib.rc('text', usetex=True)

# Parameter definitions
def param(frac=0.10):
    """ Parameters for the potential
    Input
        frac - fraction of magnitude of LJ substrate potential to be used for nonbonded interactions
    Output
        points,clist,knots,r0,D0,alpha,dbb,auwall,auau,auin,subsme,subsat,breakval
        points  - points governing the cubic bezier spline
        clist   - x end points of each cubic bezier in the spline
        knots   - knots of the cubic bezier spline
    r0,D0,alpha - the end Morse curves flanking the cubic bezier spline
        dbb     - double well bond lengths of extremal values
        auwall  - the wall lj9-3 potential
        auau    - lj substrate parameters
        auin    - lj intersubstrate parameters
        subsme  - lj substrate and mechanophore interaction
        subsat  - substrate attachment potentials
        breakval- bond breaking length
    """
    
    # L J Parameters for auau
    auau=np.zeros(4)
    #eps0 sigma cutoff r0
    auau[0]=12.306 #kcal/mol #eps0
    auau[3]=2.8847 #Angstroms #r0
    auau[1]=auau[3]/(2.0**(1.0/6.0)) #Angstroms #sigma
    auau[2]=2.5*auau[3] #cutoff

    # Intersubstrate potential
    # LJ Parameters for au au interface
    auin=np.zeros(4)
    auin[0]=auau[0]*0.5 # kcal/mol #eps0
    auin[3]=2*auau[3]# 2 inter lattice spacing #r0 #Angs
    auin[1]=auau[1]*2 #Angstroms #sigma
    auin[2]=2.5*auin[3] # cutoff

    num_density=4.0/((auau[3]*np.sqrt(2))**3)

    # Mphore points
    points=np.array([[4.0,4.5,6.0,9.0,10,11.5,12.5,12.6,13.5,15,17],
        [-10,-30,-60,-52,-30,0,0,-58,-55,-45,-25]])
    points[0,:]=points[0,:]

    knots=np.array(range(points.shape[1]+2))

    # Samples
    samples,vals,dvals,ddvals,clist=cubicbezier.cbs(points,knots,2)
    dydx=dvals[1,:]/dvals[0,:]
    d2ydx2=(ddvals[1,:]-dydx*ddvals[0,:])/(dvals[0,:]*dvals[0,:])

    # solve for morse parameters flanking the double well
    r0=np.zeros(2)
    D0=np.zeros(2)
    alpha=np.zeros(2)

    r=vals[0,0]
    a=vals[1,0]
    b=dydx[0]
    c=d2ydx2[0]
    alpha[0]=(-3*b-np.sqrt(9*b*b-8*a*c))/(4*a)
    r0[0]=r-np.log(0.5*(b+2*alpha[0]*a)/(b+alpha[0]*a))/alpha[0]
    D0[0]=a/(np.exp(-2*alpha[0]*(r-r0[0]))-2.0*np.exp(-alpha[0]*(r-r0[0])))

    r=vals[0,-1]
    a=vals[1,-1]
    b=dydx[-1]
    c=d2ydx2[-1]
    alpha[1]=(-3*b-np.sqrt(9*b*b-8*a*c))/(4*a)
    r0[1]=r-np.log(0.5*(b+2*alpha[1]*a)/(b+alpha[1]*a))/alpha[1]
    D0[1]=a/(np.exp(-2*alpha[1]*(r-r0[1]))-2.0*np.exp(-alpha[1]*(r-r0[1])))

    del r,a,b,c,samples,dvals,ddvals

    # compute positions
    # Minima 1, Minima 2, Maximum , Force for Delta E = 0
    # Pos
    # Function value
    # Derivative value
    dbb=np.zeros([3,4])
    res=sco.minimize(cubicbezier.wrapcbsyofx,7,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,0]=res.x[0]
    dbb[1,0]=res.fun
    dbb[2,0]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsyofx,13,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,1]=res.x[0]
    dbb[1,1]=res.fun
    dbb[2,1]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsnyofx,11,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,2]=res.x[0]
    dbb[1,2]=-res.fun
    dbb[2,2]=res.jac
    res=sco.minimize(cubicbezier.wrapcbsndyofx,11,args=(points,knots,clist),method='Newton-CG',jac=True)
    dbb[0,3]=res.x[0]
    dbb[1,3]=-res.fun
    dbb[2,3]=-res.jac

    # Morse potential between Gold and Me
    subsat=np.zeros(3)
    #subsat[0] = -0.70*dbb[1,0] #kcal/mol #D0
    #subsat[0] = -0.80*dbb[1,0] #kcal/mol #D0
    subsat[0] = -1.25*dbb[1,0] #kcal/mol #D0
    subsat[1]=1.066 # Angs^-1
    subsat[2]=4 #Angs

    # Non bonded interaction
    subsme=np.zeros(4)
    subsme[0]= 0.10*auau[0] #kcal/mol #eps0
    subsme[3]= 3.5 #Angstroms #distance # Vanderwaals radius of gold and carbon
    subsme[1]=subsme[3]/(2.0**(1.0/6.0)) #Angstroms #sigma
    subsme[2]=2.5*subsme[3] #cutoff

    auwall=np.zeros(4)
    #eps0 sigma cutoff walldist
    #This is generated based on the weight of the non-bonded interaction
    auwall[0]=(2.0/3.0)*np.pi*num_density*(auau[1]**3)*auau[0] #eps0
    auwall[1]=auau[1] #sigma
    auwall[2]=auau[2] #cutoff
    auwall[3]=auwall[1]/(2.5**(1.0/6.0))

    # Cutoffval for breaking bonds
    breakval=np.zeros(2)
    breakval[0]=r0[1]+2.0*np.log(2)/alpha[1]
    breakval[1]=subsat[2]+3.0*np.log(2)/subsat[1]

    return [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,auin,subsme,subsat,breakval]

# Conversion parameters
def convparam():
    """
    Function to obtain conversion parameters
    output:
    conv:
        0 - > mass (amu)
        1 - > distance (Angstrom)
        2 - > energy (kcal/mol)
        3 - > temperature (K)
        4 - > time (fs)
        5 - > force unit (nN)
        6 - > pressure (atm)
    """
    [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,auin,subsme,subsat,breakval]=param()

    # Mass of gold
    mg=196.96657 #amu #g/mole
    #useful const
    kcal=4184.0 #joule
    avagadro=6.0221413E23
    ev=1.60217657E-19 #Joule
    boltzmann=1.3806488E-23 #Joule/Kelvin
    atm=1.01325E5 #Pa

    #conversion w.r.t auau
    conv=np.zeros(7)
    conv[0]=mg #1 unit of mass is conv[0] mass of gold in amu
    conv[1]=auau[1] #1 unit of distance is conv[1] Angs
    conv[2]=auau[0] #1 unit of energy in kcal/mol
    conv[3]=auau[0]*kcal/avagadro/boltzmann # 1 unit of temperature in K
    conv[4]=np.sqrt(mg*(auau[1]**2)*1E-23/(auau[0]*kcal))*1E15 #1 unit of time in fs
    conv[5]=((conv[2]*kcal/avagadro)/(conv[1]*1E-10))*1E9 #1 unit of force in nN
    conv[6]=((conv[2]*kcal/avagadro)/(conv[1]*1E-10))/(conv[1]*conv[1]*1E-20)/atm #1 unit of pressure in atm

    return conv

# Writing the Double well potential
def writedoublewell(numsamples,potfile):
    
    """ Function to write the double well potential file
    Input:
        numsamples - number of samples
        potfile    - potential file name
    """
    
    [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,auin,subsme,subsat,breakval]=param()
    conv=convparam()

    xl=np.linspace(0.1,clist[0],numsamples,endpoint=False)
    yl=morse.morsepot(D0[0],alpha[0],r0[0],xl)
    dyl=morse.dermorsepot(D0[0],alpha[0],r0[0],xl)

    samples,vals,dvals,ddvals,clist=cubicbezier.cbs(points,knots,numsamples)
    xm=vals[0,:]
    ym=vals[1,:]
    dym=dvals[1,:]/dvals[0,:]

    xr=np.linspace(clist[-1],clist[-1]*2,2*numsamples+1)
    xr=np.delete(xr,0)
    yr=morse.morsepot(D0[1],alpha[1],r0[1],xr)
    dyr=morse.dermorsepot(D0[1],alpha[1],r0[1],xr)

    f=open(potfile,'w')
    f.write('#Double Well Potential\n\n')
    f.write('DOUBLE\n')
    f.write('N %d \n\n'%(numsamples+2*numsamples+xm.shape[0]))
    count=0
    for iind in range(numsamples):
        count=count+1
        f.write('%d %4.5f %4.5f %4.5f\n'%(count,xl[iind]/conv[1],yl[iind]/conv[2],-dyl[iind]*conv[1]/conv[2]))
    for iind in range(xm.shape[0]):
        count=count+1
        f.write('%d %4.5f %4.5f %4.5f\n'%(count,xm[iind]/conv[1],ym[iind]/conv[2],-dym[iind]*conv[1]/conv[2]))
    for iind in range(2*numsamples):
        count=count+1
        f.write('%d %4.5f %4.5f %4.5f\n'%(count,xr[iind]/conv[1],yr[iind]/conv[2],-dyr[iind]*conv[1]/conv[2]))
    f.close()
    return count

# Plotting the Double well potential
# def plotdoublewell(numsamples,plotfile):
#
#     [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,auin,subsme,subsat,breakval]=paramconvfac()
#
#     xl=np.linspace(0.1,clist[0],numsamples,endpoint=False)
#     yl=morse.morsepot(D0[0],alpha[0],r0[0],xl)
#     dyl=morse.dermorsepot(D0[0],alpha[0],r0[0],xl)
#
#     samples,vals,dvals,ddvals,clist=cubicbezier.cbs(points,knots,numsamples)
#     xm=vals[0,:]
#     ym=vals[1,:]
#     dym=dvals[1,:]/dvals[0,:]
#
#     xr=np.linspace(clist[-1],clist[-1]*2,2*numsamples+1)
#     xr=np.delete(xr,0)
#     yr=morse.morsepot(D0[1],alpha[1],r0[1],xr)
#     dyr=morse.dermorsepot(D0[1],alpha[1],r0[1],xr)
#
#     plt.figure(1,figsize=(12,8))
#     plt.plot(xl,yl,linewidth=3,color='blue')
#     plt.plot(xm,ym,linewidth=3,color='blue')
#     plt.plot(xr,yr,linewidth=3,color='blue')
#     plt.xlabel(r'Distance \AA')
#     plt.ylabel(r'Potential kcal/mol')
#     plt.grid(True)
#     plt.tight_layout()
#
#     plt.savefig(plotfile,dpi=128,format='png')
