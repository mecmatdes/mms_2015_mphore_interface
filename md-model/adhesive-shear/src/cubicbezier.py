#!/usr/bin/env python

import numpy as np
import scipy.optimize as sco
import numpy.matlib 

# Cubic Bezier Spline for double well potential

# Sampling cubic spline with equi spaced samples
def cbs(points,knots,num_samples):

    """ Return a sampling of cubic bezier splines: (x,y),(dx,dy),(d2x,d2y),endpoints
        Input:
            points      : The control points stored as [points[:,0],points[:,0],\cdots,points[:,n-1]]
            knots       : Increasing ordered numbers knots[0]<knots[1]<knots[2]<\cdots<knots[n]<knots[n+1]
            num_samples : Number of samples required between each of the knots (starting from knots[1] \cdots knots[n])
        Output:
            samples :   points in knot space where samples are drawn from
            vals    :   x,y values aranged as [x;y] - [2,num_samples*(points.shape[1]-3)]
            dvals   :   dx/dt, dy/dt values aranged as [dx/dt;dy/dt] - [2,num_samples*(points.shape[1]-3)]
            ddvals  :   d2x/dt2,d2y/dt2 values aranged as [d2x/dt2;d2y/dt2] - [2,num_samples*(points.shape[1]-3)]
            clist   :   end points of each cubic bezier curve [x1,x2,...,xn,xn+1] - [point.shape[1]-3+1]
    """

    if(knots.shape[0]-2!=points.shape[1]):
        print("Number of knots is not 2 less than the number of points+2")
        return 0

    if(points.shape[1]<4):
        print("Number of points needs to be greater than or equal to 4")
        return 0

    # Number of curves constituting the cubic bezier spline
    num_curves=points.shape[1]-3

    # Number of samples
    samples=np.zeros(num_curves*num_samples)

    # Values, Derivative, Double Derivatives
    vals=np.zeros([2,num_curves*num_samples])
    dvals=np.zeros([2,num_curves*num_samples])
    ddvals=np.zeros([2,num_curves*num_samples])
    clist=np.zeros(num_curves+1)

    for ind in range(num_curves):

        # For every curve neglect the end value because it will be obtained from the next curve
        # Avoids repetition
        endstate=False

        # Except For the last curve, get the end value at the third knot
        if(ind==(num_curves-1)):
            endstate=True
        
        # Cubic bezier curve
        [s,p,dp,ddp]=cbezier(points[:,ind:ind+4],knots[ind:ind+6],num_samples,endstate)

        # End points for locating curves
        clist[ind]=p[0,0]
        
        # Last end point        
        if(ind==(num_curves-1)):
            clist[ind+1]=p[0,-1]
        
        # Store values
        samples[ind*num_samples:(ind+1)*num_samples]=s
        vals[:,ind*num_samples:(ind+1)*num_samples]=p
        dvals[:,ind*num_samples:(ind+1)*num_samples]=dp
        ddvals[:,ind*num_samples:(ind+1)*num_samples]=ddp

    
    return samples,vals,dvals,ddvals,clist

# Cubic bezier curve evaluation between knots[2] and knots[3]
def cbezier(points,knots,num_samples,endstate=True):
    """Return a sampling of the cubic bezier curve for values of samples ranging from knots[2] to knots[3]
    Input:
        points        : 4 points stored as [points[:,0],points[:,1],points[:,2],points[:,3]]
        knots         : 6 knots, in increasing order as knots[0]<knots[1]<knots[2]<knots[3]<knots[4]<knots[5]
        num_samples   : Number of samples required between knots[2] and knots[3]
        endstate      : Whether knots[3] should be included or not True or False
    Output:
        samples :   points in knot space where samples are drawn from
        p       :   x,y values aranged as [x;y] - [2,num_samples]
        dp      :   dx/dt, dy/dt values aranged as [dx/dt;dy/dt] - [2,num_samples]
        ddp     :   d2x/dt2,d2y/dt2 values aranged as [d2x/dt2;d2y/dt2] - [2,num_samples]
    """

    if(knots.shape[0]!=6):
        print("Number of knots needs to be 6 to form a cubic bezier")

    if(points.shape[1]!=4):
        print("Number of points needs to be 4 to form a cubic bezier")

    # Linearly spaced samples
    samples=np.linspace(knots[2],knots[3],num_samples,endpoint=endstate)

    # For vectorization
    a1=np.transpose(np.matlib.repmat(points[:,0],num_samples,1))
    a2=np.transpose(np.matlib.repmat(points[:,1],num_samples,1))
    a3=np.transpose(np.matlib.repmat(points[:,2],num_samples,1))
    a4=np.transpose(np.matlib.repmat(points[:,3],num_samples,1))

    # Linear interpolation
    den=(knots[3]-knots[0])
    p11=(knots[3]-samples)*a1+(samples-knots[0])*a2
    p11=p11/den
    dp11=(a2-a1)/den

    den=(knots[4]-knots[1])
    p21=(knots[4]-samples)*a2+(samples-knots[1])*a3
    p21=p21/den
    dp21=(a3-a2)/den

    den=(knots[5]-knots[2])
    p31=(knots[5]-samples)*a3+(samples-knots[2])*a4
    p31=p31/den
    dp31=(a4-a3)/den

    # Quadratic interpolation
    den=(knots[3]-knots[1])
    p12=(knots[3]-samples)*p11+(samples-knots[1])*p21
    p12=p12/den
    dp12=(p21-p11)+(knots[3]-samples)*dp11+(samples-knots[1])*dp21
    dp12=dp12/den
    ddp12=2*(dp21-dp11)/den

    den=(knots[4]-knots[2])
    p22=(knots[4]-samples)*p21+(samples-knots[2])*p31
    p22=p22/den
    dp22=(p31-p21)+(knots[4]-samples)*dp21+(samples-knots[2])*dp31
    dp22=dp22/den
    ddp22=2*(dp31-dp21)/den

    # Cubic interpolation
    den=(knots[3]-knots[2])
    p=(knots[3]-samples)*p12+(samples-knots[2])*p22
    p=p/den
    dp=(p22-p12)+(knots[3]-samples)*dp12+(samples-knots[2])*dp22
    dp=dp/den
    ddp=(dp22-dp12)+(dp22-dp12)+(knots[3]-samples)*ddp12+(samples-knots[2])*ddp22
    ddp=ddp/den

    return [samples,p,dp,ddp]

#wrapper for minimization of potential
def wrapcbsyofx(x,points,knots,clist):
    """Return function value and derivative
            Input:
                x       :   x value at which the function value and the derivative are required
                points  :   The control points stored as [points[:,0],points[:,0],\cdots,points[:,n-1]]
                knots   :   Increasing ordered numbers knots[0]<knots[1]<knots[2]<\cdots<knots[n]<knots[n+1]
                clist   :   end points of each cubic bezier curve [x1,x2,...,xn,xn+1] - [point.shape[1]-3+1]
            Output:
                y       :   y value at x
                dy/dx   :   dy/dx value at x
    """
    [knotval,p,dp,ddp]=cbsyofx(points,knots,clist,x)
    return [p[1],dp[1]/dp[0]]

#wrapper for maximization of potential
def wrapcbsnyofx(x,points,knots,clist):
    """Return negative function value and negative derivative
            Input:
                x       :   x value at which the negative function value and the negative derivative are required
                points  :   The control points stored as [points[:,0],points[:,0],\cdots,points[:,n-1]]
                knots   :   Increasing ordered numbers knots[0]<knots[1]<knots[2]<\cdots<knots[n]<knots[n+1]
                clist   :   end points of each cubic bezier curve [x1,x2,...,xn,xn+1] - [point.shape[1]-3+1]
            Output:
                -y       :   negative y value at x
                -dy/dx   :   negative dy/dx value at x
    """
    [knotval,p,dp,ddp]=cbsyofx(points,knots,clist,x)
    return [-p[1],-dp[1]/dp[0]]

#wrapper function for minimization of the derivative
def wrapcbsdyofx(x,points,knots,clist):
    """Return derivative of function value and double derivative of function value
            Input:
                x       :   x value at which the derivative value and the double derivative are required
                points  :   The control points stored as [points[:,0],points[:,0],\cdots,points[:,n-1]]
                knots   :   Increasing ordered numbers knots[0]<knots[1]<knots[2]<\cdots<knots[n]<knots[n+1]
                clist   :   end points of each cubic bezier curve [x1,x2,...,xn,xn+1] - [point.shape[1]-3+1]
            Output:
                dy/dx   :   dy/dx value at x
                d2y/dx2 :   d2y/dx2 value at x
    """
    [knotval,p,dp,ddp]=cbsyofx(points,knots,clist,x)
    dydx=dp[1]/dp[0]
    d2ydx2=(ddp[1]-dydx*ddp[0])/(dp[0]*dp[0])
    return [dydx,d2ydx2]

#wrapper function for maximization of the derivative
def wrapcbsndyofx(x,points,knots,clist):
    """Return negative derivative of function value and negative double derivative of function value
            Input:
                x       :   x value at which the negative derivative of the function value and the negative double derivative are required
                points  :   The control points stored as [points[:,0],points[:,0],\cdots,points[:,n-1]]
                knots   :   Increasing ordered numbers knots[0]<knots[1]<knots[2]<\cdots<knots[n]<knots[n+1]
                clist   :   end points of each cubic bezier curve [x1,x2,...,xn,xn+1] - [point.shape[1]-3+1]
            Output:
                dy/dx   :   negative dy/dx value at x
                d2y/dx2 :   negative d2y/dx2 value at x
    """
    [knotval,p,dp,ddp]=cbsyofx(points,knots,clist,x)
    dydx=dp[1]/dp[0]
    d2ydx2=(ddp[1]-dydx*ddp[0])/(dp[0]*dp[0])
    return [-dydx,-d2ydx2]

# In the cubic spline identify function values given x
def cbsyofx(points,knots,clist,x):

    """Given x determine the knot values, function values, and the derivatives
            Input:
                points  :   The control points stored as [points[:,0],points[:,0],\cdots,points[:,n-1]]
                clist   :   end points of each cubic bezier curve [x1,x2,...,xn,xn+1] - [point.shape[1]-3+1]
                knots   :   Increasing ordered numbers knots[0]<knots[1]<knots[2]<\cdots<knots[n]<knot
                x       :   x value at which the negative derivative of the function value and the negative double derivative are required
            Output:
                knotval :   knotvalue corressponding to x
                p       :   [x;y]
                dp      :   [dx;dy]
                ddp     :   [ddx;ddy]
    """


    if(knots.shape[0]-2!=points.shape[1]):
        print("Number of knots is not 2 less than the number of points+2")
        return 0

    if(points.shape[1]<4):
        print("Number of points needs to be greater than or equal to 4")
        return 0

    num_curves=clist.shape[0]-1
    # locate which cubicbezier curve it exists in
    cind=-1
    for ind in range(num_curves):
        if(x<clist[ind+1] and x>=clist[ind]):
            cind=ind
            break;

    # Use numerical routine ridder to get an accurate estimation of knot value
    knotval=sco.brentq(wrapcbezxofknot,knots[cind+2],knots[cind+3],args=(points[:,cind:cind+4],knots[cind:cind+6],x))

    # Get the values at the knot value
    [p,dp,ddp]=cbezierfofknot(points[:,cind:cind+4],knots[cind:cind+6],knotval)
    
    return [knotval,p,dp,ddp]

# wrapper function for cbezierfofknot for root finding for the value of the knot
def wrapcbezxofknot(knotval,points,knots,x):
    """Return the difference between given x and obtained x evaluated at knotval
        Input:
            knotval     : knot value at which the evaluation is desired
            points      : 4 points stored as [points[:,0],points[:,1],points[:,2],points[:,3]]
            knots       : 6 knots, in increasing order as knots[0]<knots[1]<knots[2]<knots[3]<knots[4]<knots[5]
            x           : given x value
        Output:
            p[0]-x
    """
    # Get the values at the knot value
    if(knots.shape[0]!=6):
        print("Number of knots needs to be 6 to form a cubic bezier")
        return 0

    if(points.shape[1]!=4):
        print("Number of points needs to be 4 to form a cubic bezier")
        return 0

    if(knotval<knots[2] or knotval>knots[3]):
        print("knotval not between knot2 and knot 3")
        return 0

    # Linear
    den=knots[3]-knots[0]
    p11=(knots[3]-knotval)*points[:,0]+(knotval-knots[0])*points[:,1]
    p11=p11/den
    
    den=knots[4]-knots[1]
    p21=(knots[4]-knotval)*points[:,1]+(knotval-knots[1])*points[:,2]
    p21=p21/den
    
    den=knots[5]-knots[2]
    p31=(knots[5]-knotval)*points[:,2]+(knotval-knots[2])*points[:,3]
    p31=p31/den
    
    # Quadratic
    den=knots[3]-knots[1]
    p12=(knots[3]-knotval)*p11+(knotval-knots[1])*p21
    p12=p12/den
    
    den=(knots[4]-knots[2])
    p22=(knots[4]-knotval)*p21+(knotval-knots[2])*p31
    p22=p22/den
    
    # Cubic
    den=(knots[3]-knots[2])
    p=(knots[3]-knotval)*p12+(knotval-knots[2])*p22
    p=p/den
    
    return p[0]-x

# Evaluate the cubic bezier at knotval
def cbezierfofknot(points,knots,knotval):
    """Return the cubic bezier interpolant, derivative, double derivative evaluated at knotval
        Input:
            points      : 4 points stored as [points[:,0],points[:,1],points[:,2],points[:,3]]
            knots       : 6 knots, in increasing order as knots[0]<knots[1]<knots[2]<knots[3]<knots[4]<knots[5]
            knotval     : knot value at which the evaluation is desired
        Output:
            p       :   [x;y]
            dp      :   [dx;dy]
            ddp     :   [ddx;ddy]
   """
    if(knots.shape[0]!=6):
        print("Number of knots needs to be 6 to form a cubic bezier")
        return 0

    if(points.shape[1]!=4):
        print("Number of points needs to be 4 to form a cubic bezier")
        return 0

    if(knotval<knots[2] or knotval>knots[3]):
        print("knotval not between knot2 and knot 3")
        return 0

    # Linear
    den=knots[3]-knots[0]
    p11=(knots[3]-knotval)*points[:,0]+(knotval-knots[0])*points[:,1]
    p11=p11/den
    dp11=(points[:,1]-points[:,0])/den

    den=knots[4]-knots[1]
    p21=(knots[4]-knotval)*points[:,1]+(knotval-knots[1])*points[:,2]
    p21=p21/den
    dp21=(points[:,2]-points[:,1])/den

    den=knots[5]-knots[2]
    p31=(knots[5]-knotval)*points[:,2]+(knotval-knots[2])*points[:,3]
    p31=p31/den
    dp31=(points[:,3]-points[:,2])/den

    # Quadratic
    den=knots[3]-knots[1]
    p12=(knots[3]-knotval)*p11+(knotval-knots[1])*p21
    p12=p12/den
    dp12=(p21-p11)+(knots[3]-knotval)*dp11+(knotval-knots[1])*dp21
    dp12=dp12/den
    ddp12=2*(dp21-dp11)/den

    den=(knots[4]-knots[2])
    p22=(knots[4]-knotval)*p21+(knotval-knots[2])*p31
    p22=p22/den
    dp22=(p31-p21)+(knots[4]-knotval)*dp21+(knotval-knots[2])*dp31
    dp22=dp22/den
    ddp22=2*(dp31-dp21)/den

    # Cubic
    den=(knots[3]-knots[2])
    p=(knots[3]-knotval)*p12+(knotval-knots[2])*p22
    p=p/den
    dp=(p22-p12)+(knots[3]-knotval)*dp12+(knotval-knots[2])*dp22
    dp=dp/den
    ddp=(dp22-dp12)+(dp22-dp12)+(knots[3]-knotval)*ddp12+(knotval-knots[2])*ddp22
    ddp=ddp/den

    return [p,dp,ddp]
