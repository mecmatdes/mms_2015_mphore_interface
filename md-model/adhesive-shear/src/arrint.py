#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of "ahesiveshear" package that builds the substrate for the md-model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# adhesiveshear is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# adhesiveshear is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.


import numpy as np
import subprocess

# Arrange Substrate Atoms
def arrsubsatoms(configfil1,configfil2, scale,shift):
    """
    Arrange substrate atoms
    Input:
        configfil1 - configuration file 1
        configfil2 - configuration file 2

        scale      - length scale, used to scale the configuration
        shift      -
            shift[0] - shift the lower substrate
            shift[1] - interfacial shift
            shift[1] - shift the upper substrate
    Output:
        atomset - atom coordinates in x y z
        bounds  - [[0 xhi]
                   [0 yhi]
                   [0 zhi]]
        natm    - [natm[0]  number of atoms in bottom substrate
                - natm[1]]  number of atoms in top substrate
        """
    #Number molecule x y z
    # Load number of atoms
    # Load box size
    natm=np.zeros(2,dtype=np.int)
    box=np.zeros([3,4])
    f1=open(configfil1,'r')
    f1.readline()
    f1.readline()
    f1.readline()
    natm[0]=np.int(f1.readline())
    f1.readline()
    line=f1.readline().split()
    box[0,0]=np.float(line[0])*scale
    box[0,1]=np.float(line[1])*scale
    line=f1.readline().split()
    box[1,0]=np.float(line[0])*scale
    box[1,1]=np.float(line[1])*scale
    line=f1.readline().split()
    box[2,0]=np.float(line[0])*scale
    box[2,1]=np.float(line[1])*scale
    f1.close()

    f2=open(configfil2,'r')
    f2.readline()
    f2.readline()
    f2.readline()
    natm[1]=np.int(f2.readline())
    f2.readline()
    line=f2.readline().split()
    box[0,2]=np.float(line[0])*scale
    box[0,3]=np.float(line[1])*scale
    line=f2.readline().split()
    box[1,2]=np.float(line[0])*scale
    box[1,3]=np.float(line[1])*scale
    line=f2.readline().split()
    box[2,2]=np.float(line[0])*scale
    box[2,3]=np.float(line[1])*scale
    f2.close()

    # Load content of the files
    config1=np.loadtxt(configfil1,usecols=[0,1,2,3],skiprows=9,ndmin=2)
    config2=np.loadtxt(configfil2,usecols=[0,1,2,3],skiprows=9,ndmin=2)

    config1[:,1:4]=config1[:,1:4]*scale
    config2[:,1:4]=config2[:,1:4]*scale

    # First substrate from bottom up
    # Second substrate from top down

    # Orient every atom from the origin of its respective box
    config1[:,1]=config1[:,1]-box[0,0]
    config1[:,2]=config1[:,2]-box[1,0]
    config1[:,3]=box[2,1]-config1[:,3]

    config2[:,1]=config2[:,1]-box[0,2]
    config2[:,2]=config2[:,2]-box[1,2]
    config2[:,3]=box[2,3]-config2[:,3]

    # Arrange the boxes
    # Compute the bounds
    bounds=np.zeros([3,2])
    bounds[0,1]=max(box[0,1]-box[0,0],box[0,3]-box[0,2])
    bounds[1,1]=max(box[1,1]-box[1,0],box[1,3]-box[1,2])
    bounds[2,1]=box[2,3]-box[2,2]+box[2,1]-box[2,0]+shift[0]+shift[1]+shift[2]
                                                    #Wall Separation Wall

    atomset=np.zeros([natm[0]+natm[1],3])

    # atomset
    # x y z
    # bottom half
    for iind in range(natm[0]):
        atomset[iind,:]=config1[iind,1:4]+np.array([0,0,shift[0]])
                                            # Wall height
    #top half
    # x y z
    for iind in range(natm[1]):
        atomset[natm[0]+iind,0:2]=config2[iind,1:3]
        atomset[natm[0]+iind,2]=bounds[2,1]-config2[iind,3]-shift[2]
                                # Place from above #Wall height
    return [atomset,bounds,natm]
