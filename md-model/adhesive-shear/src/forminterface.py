#!/usr/bin/env python

# Copyright 2015 Mechanics for Material Design Lab,
# Sibley School of Mechanical and Aerospace Engineering,
# Cornell, Ithaca
# Author: Meenakshi Sundaram
# Contact: mm2422 at cornell dot edu

# This file is part of "adhesiveshear" package that builds the substrate for the md-model in the paper
# "Computational investigation of shear driven mechanophore activation at interfaces"
# by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
# Cite the work using the following Bibtex entry
# @article{manivannan2015computational,
# title={Computational investigation of shear driven mechanophore activation at interfaces},
# author={Manivannan, Sundaram Meenakshi and Silberstein, Meredith N},
# journal={Extreme Mechanics Letters},
# year={2015},
# publisher={Elsevier}
# }

# adhesiveshear is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# adhesiveshear is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with double-well-potential.
# If not, see <http://www.gnu.org/licenses/>.

import sys,os,shutil
import numpy as np

import potparam
import arrint

import matplotlib
matplotlib.use('Agg')
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
matplotlib.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica'],'size':32})
matplotlib.rc('text', usetex=True)


if __name__ == '__main__':

    if(len(sys.argv)<3):
        print("ERROR: USAGE interface.py config1fil config2fil")
        exit()

    # Potential parameters + conversion factors #Write potential
    [points,clist,knots,r0,D0,alpha,dbb,auwall,auau,auin,subsme,subsat,breakval]=potparam.param()
    conv=potparam.convparam()
    numsamp=potparam.writedoublewell(201,'pot.dat')

    # Read files and store the atoms
    shift=np.array([auwall[3],2*auau[3]*np.sqrt(2.0/3.0),auwall[3]])
    [atomset,bounds,natm]=arrint.arrsubsatoms(sys.argv[1],sys.argv[2],conv[1],shift)

    # Number of atom types
    nattypes=2
    # Number of atoms
    numatoms=natm[0]+natm[1]
    # Number of bond types
    nbndtypes=0
    nbnds=0
    # Mass
    mass=np.array([conv[0],conv[0]])
    # Timestep
    timestep=1 #E-15
    # Damp time for equilibration after pe minimization
    damp=timestep*np.array([100])
    # Steps
    # Equilibration Shear
    nsteps=np.array([80000])
    # Frames
    nframes=100
    # Temperature
    temp=300 #K

    # atoms, bonds, atom types, bond types
    f=open('interface.dat','w')
    f.write('This file contains the position of the atoms an mphores for the interface\n')
    f.write('%d atoms\n'%(numatoms))
    f.write('%d bonds\n'%(nbnds))
    f.write('%d atom types\n'%(nattypes))
    f.write('%d bond types\n'%(nbndtypes))
    f.write('%f %f xlo xhi\n'%(0,bounds[0,1]/conv[1]))
    f.write('%f %f ylo yhi\n'%(0,bounds[1,1]/conv[1]))
    f.write('%f %f zlo zhi\n'%(0,bounds[2,1]/conv[1]))

    f.write('\n')
    f.write('Masses\n')
    f.write('\n')
    for jind in range(nattypes):
        f.write('%d %f\n'%(jind+1,mass[jind]/conv[0]))
    f.write('\n')

    f.write('Atoms\n')
    f.write('\n')

    # Storage Order
    # id moleculeid type x y z
    molid=1
    attype=1
    for jind in range(natm[0]):
        f.write('%d %d %d %f %f %f\n'%(jind+1,molid,attype,atomset[jind,0]/conv[1],atomset[jind,1]/conv[1],atomset[jind,2]/conv[1]))

    molid=2
    attype=2
    for jind in range(natm[0],natm[0]+natm[1]):
        f.write('%d %d %d %f %f %f\n'%(jind+1,molid,attype,atomset[jind,0]/conv[1],atomset[jind,1]/conv[1],atomset[jind,2]/conv[1]))

    f.close()

    f=open('in.lammps','w')
    f.write("""# Initialization
clear

# Choice of units - relative units
units lj

# 3 dimensional problem
dimension 3

# We are simulating the interface
# so it is periodic on two sides and not on the third side
boundary p p s

# Type of atoms involved
atom_style bond

# Mapping style
atom_modify map array

# Read Data
read_data interface.dat

# Neighbor set up
neighbor 1.0 bin
neigh_modify delay 1 check yes
comm_modify mode single cutoff %f vel yes
#communicate single cutoff -- vel yes

# Time Step
reset_timestep 0
timestep %f

"""%(2*np.max(breakval)/conv[1],timestep/conv[4]))

    # Note: Minimal interaction between substrates
    f.write("""# Force Fields
pair_style lj/cut %f
pair_coeff 1 1 %f %f
pair_coeff 1 2 %f %f %f
pair_coeff 2 2 %f %f
"""%(auau[2]/conv[1],auau[0]/conv[2],auau[1]/conv[1],auin[0]/conv[2],auin[1]/conv[1],auin[2]/conv[1],auau[0]/conv[2],auau[1]/conv[1]))

    f.write("""# Create the walls
fix wallhilo all wall/lj93 zlo EDGE %f %f %f zhi EDGE %f %f %f
fix_modify wallhilo energy yes

"""%(auwall[0]/conv[2],auwall[1]/conv[1],auwall[2]/conv[1],auwall[0]/conv[2],auwall[1]/conv[1],auwall[2]/conv[1]))

    f.write("""thermo 1
thermo_style custom step pe ke temp lz
thermo_modify norm no

# Minimize Potential Energy
min_style fire
minimize 1.0e-14 1.0e-16 1000 10000
""")

    f.write("""# Equilibrate
velocity all create %f 2313
restart 10000 interface.restart
dump 101 all custom %d interface.conf.dat id type x y z
dump_modify 101 sort id
fix 2 all nvt temp %f %f %f tloop 10
run %d
unfix 2
unfix wallhilo
"""%(temp/conv[3],nsteps/nframes,temp/conv[3],temp/conv[3],damp/conv[4],nsteps))

    f.close()

